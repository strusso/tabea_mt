# ML for flood simulation

## Installation 
1. Start by cloning the repository using:
```sh
git@gitlab.ethz.ch:strusso/tabea_mt.git
cd tabea_mt
```

2. Check that the data is present (might not be necessary):
```sh
git lfs install
git lfs pull
```

3. Install the package. Create your virtual environment and run
```sh
pip install -e .
```
Alternatively, use `pipenv`:
```sh 
pipenv shell
pipenv install
```

## Getting started
0. Download the data from catchemnt 709 from drive and put each event (tr_) into a separate folder. Drive link:https://drive.google.com/drive/u/1/folders/1W6TDPE068KMld3HxcJwxLUkhNfIbl1WM
1. Open the .env_example file and set up your data folders 
2. The dataset generation is done in the notebook `notebooks/data_preprocessing.ipynb`. Follow the instructions there. Alternatively, you only need to run the function `build_dataset` from `mlflood.preprocess`.
3. Train and test the models by running train.sh and test.sh
4. Visualization of test results is done in the notebook `notebooks/test_visualization.ipynb`
