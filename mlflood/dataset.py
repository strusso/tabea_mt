import numpy as np
import torch
from torch.nn.functional import pad
import torch.utils.data as torchdata
from mlflood.conf import PATH_GENERATED, PATH_DATA
from pathlib import Path
import h5py
from scipy import ndimage
import json
import cv2

def max_wd(catchment_num):
    """
    if train_dataset:
    max_wd = 0
    for i in range(len(waterdepth)):
        if waterdepth[i].max() > max_wd:
            max_wd = waterdepth[i].max()
    """
    path_max_json =  Path(PATH_DATA) / Path(catchment_num) / Path("normalization.json")
    with open(path_max_json, "r") as f:
        d = json.load(f)

    max_wd = d["max_wd_train"]
    return max_wd

def max_rainfall(catchment_num):
    """
    if train_dataset:
    max_rainfall = 0
    for i in range(len(rainfall_events)):
        if rainfall_events[i].max() > max_rainfall:
            max_rainfall = rainfall_events[i].max()
    """
    path_max_json =  Path(PATH_DATA) / Path(catchment_num) / Path("normalization.json")
    with open(path_max_json, "r") as f:
        d = json.load(f)
    max_rainfall = d["max_rainfall_train"]
    
    return max_rainfall

def normalize_waterdepth(waterdepth, max_wd):
    for i in range(len(waterdepth)):
        waterdepth[i] = waterdepth[i]/max_wd
    return waterdepth 

def unnormalize_waterdepth(waterdepth, max_wd):
    # this function is called for one event (tensor) instead of a list 
    waterdepth = waterdepth*max_wd
    return waterdepth 

def normalize_rainfall(rainfall_events, max_rainfall):
    for i in range(len(rainfall_events)):
        rainfall_events[i] = rainfall_events[i]/max_rainfall
    return rainfall_events    

def unnormalize_rainfall(rainfall_events, max_rainfall):
    for i in range(len(rainfall_events)):
        rainfall_events[i] = rainfall_events[i]*max_rainfall
    return rainfall_events    

def load_dataset(catchment_num = "toy", catchment_valid=None, **catchment_kwargs):
    if catchment_valid is None:
        catchment_valid=catchment_num
    train_dataset = MyCatchment(PATH_GENERATED / Path(catchment_num+"-train.h5"), random_patches=True, catchment_num=catchment_num, **catchment_kwargs)
    valid_dataset = MyCatchment(PATH_GENERATED / Path(catchment_valid+"-val.h5"), random_patches=False, catchment_num=catchment_num, **catchment_kwargs)
    return train_dataset, valid_dataset

def load_test_dataset(catchment_num = "sims", **catchment_kwargs):
    dataset = MyCatchment(PATH_GENERATED / Path(catchment_num+"-test.h5"), catchment_num=catchment_num, **catchment_kwargs)
    return dataset


def dataloader_tr_val(train_dataset, valid_dataset, catchment_num = "toy", batch_size = 8):
    
    dataloaders_train = torchdata.DataLoader(train_dataset, 
                                            batch_size=batch_size,
                                            shuffle=True,
                                            drop_last=True)
    
    dataloaders_valid = torchdata.DataLoader(valid_dataset, 
                                        batch_size=batch_size,
                                        shuffle=True,
                                        drop_last=True)
    
    return dataloaders_train, dataloaders_valid

def dataloader_test(test_dataset,catchment_num = "toy", batch_size = 8):
    
    dataloaders_test = torchdata.DataLoader(test_dataset, 
                                            batch_size=batch_size,
                                            shuffle=True,
                                            drop_last=True)

    return dataloaders_test

def pad_borders(waterdepth, dem, mask, border_size):
    dem = pad(dem, 4*[border_size], mode='constant', value=-1)
    mask = pad(mask, 4*[border_size], mode='constant', value=False) 
    dem[mask==False] = -1
    waterdepth = pad(waterdepth, 4*[border_size], mode='constant', value=0)
    return waterdepth, dem, mask

def get_topo_index(catchment_num):
    # load the topographic index map
    # TO DO: include this part in build dataset instead of here?
    path_topo_index =  Path(PATH_DATA) / Path(catchment_num) / Path("topo_index.npy")  
    topo_index=np.load(path_topo_index)
    return torch.as_tensor(topo_index)

def normalize_diff_dem(dx, mask_eroded):
    # range of diff_dem values within the eroded mask
    dx_values = torch.masked_select(dx, mask_eroded)
    # normalize within this range
    dx = (dx-dx_values.min())/(dx_values.max()-dx_values.min())
    # remove high values at the edges -> 0
    return torch.where((dx<0.0) | (dx>1.0), torch.tensor(0, dtype=dx.dtype), dx) 

def build_diff_dem(dem, mask):
    ''''
    Inputs: 
    dem = digital elevation model (tensor)
    mask = catchment mask (tensor)
    Output:
    diff_dem = tensor with gradient computed by shifting dem in +/- x/y direction
    normalized by the min and max values within the catchment
    '''
    px, py = dem.shape
    dx1 = torch.unsqueeze(torch.diff(dem, prepend=torch.tensor([[-1]*py]), dim=0), 0)
    dx2 = torch.unsqueeze(torch.diff(dem, append=torch.tensor([[-1]*py]), dim=0), 0)
    dy1 = torch.unsqueeze(torch.diff(dem, prepend=torch.tensor([[-1]]*px), dim=1), 0)
    dy2 = torch.unsqueeze(torch.diff(dem, append=torch.tensor([[-1]]*px), dim=1), 0)

    # use an eroded mask to avoid large diff_dem values at the edge
    mask_eroded=ndimage.binary_erosion(mask, structure=np.ones((5,5)))
    mask_eroded=torch.from_numpy(mask_eroded)
    
    # normalize each diff_dem channel
    dx1 = normalize_diff_dem(dx1, mask_eroded)
    dx2 = normalize_diff_dem(dx2, mask_eroded)
    dy1 = normalize_diff_dem(dy1, mask_eroded)
    dy2 = normalize_diff_dem(dy2, mask_eroded)

    return torch.cat([dx1,dx2,dy1,dy2])


class MyCatchment(torch.utils.data.Dataset):
    
    def __init__(self, h5file, catchment_num="709", tau=0.5, upsilon=0, timestep=1, sample_type="single", dim_patch=64, 
    fix_indexes=False, border_size=0, normalize_output = False, use_diff_dem=True, use_topo_index=True, 
    random_patches=True, lead_time=1, convlstm = None, accumulate_rain=True, add_dry_timesteps=None, ts_out = 1, precision = 32):

        ''''
        Initialization

        Inputs
        h5file = data structure with WD maps, DEM, mask
        tau = minimum fraction of image which must not be masked
        upsilon = 
        timestep = number of input images
        sample_type = full if input is not cropped into patches
        dim_patch = patch dimension
        fix_indexes = 
        border_size =
        normalize_output = 
        use_diff_dem=use 4 input features that represent the slope in 4 directions
        use_topo_index=use 1 input feature corresponding to the topographic index
        random_patches=True 
        lead_time=use as a target the WD map that is lead timesteps ahead
        accumulate_rainfall = WD at lead timesteps ahead is predicted, if True input: sum of forecasted rainfall in this period
            False: series of rainfall inputs, representing the hydrograph
        accumulate_rain=model input sum of rainfall or rainfall timeseries, 
        ts_out = number of output predicted waterdepths

        '''
        
        # this is not the way it should be, but let us fix it once we go for more than one time step
        if not(sample_type in ["single",  "full"]):
            raise ValueError("Unknown sample_type")
        self.timestep = timestep
        self.border_size = border_size
        self.do_pad = True
        self.use_diff_dem = use_diff_dem
        self.use_topo_index = use_topo_index
        self.random_patches = random_patches
        self.add_dry_timesteps = add_dry_timesteps
        self.ts_out = ts_out
        self.lead_time = lead_time
        self.convlstm = convlstm
        # timestep>1 or lead_time>1 require more rain channels as inputs
        self.accumulate_rain=accumulate_rain
        self.n_rain_channels = (1 if accumulate_rain else max(timestep, lead_time))
        self.n_wd_channels = timestep
        self.n_topo_channels = (4 if use_diff_dem else 0) + (1 if use_topo_index else 0)

        print(f"Load file: {h5file}")
        self.h5file = h5py.File(h5file, "r")
        keys = sorted(self.h5file.keys())
        self.dem = self.pad_borders(torch.tensor(self.h5file["dem"][()]).float(), -1)
        self.dem_mask = self.pad_borders(torch.tensor(self.h5file["mask"][()]).bool(), False)
        self.dem[self.dem_mask==False] = -1
        self.topo_index = get_topo_index(catchment_num)
        self.diff_dem = build_diff_dem(self.dem, self.dem_mask)
        self.input_channels = 1 + self.n_rain_channels + self.n_wd_channels + self.n_topo_channels
        
        self.rainfall_events = []
        for k in filter(lambda x: "rainfall_events"==x[:15], keys ):
            self.rainfall_events.append(torch.tensor(self.h5file[k][()]).float())
        self.max_rainfall = max_rainfall(catchment_num)
        self.rainfall_events = normalize_rainfall(self.rainfall_events, self.max_rainfall)

        self.waterdepth = []
        for k in filter(lambda x: "waterdepth"==x[:10], keys):
            self.waterdepth.append(self.pad_borders(torch.from_numpy(self.h5file[k][()]).float(), 0))
        self.max_wd = max_wd(catchment_num)
        self.waterdepth = normalize_waterdepth(self.waterdepth, self.max_wd)

        self.N_events = len(self.waterdepth)
        self.px = self.waterdepth[0].shape[1]
        self.py = self.waterdepth[0].shape[2]
        self.T_steps = [len(x) for x in self.rainfall_events] 

        if self.add_dry_timesteps is not None:
            # concatenate zero WD maps and rainfall = 0 prior to the start of the rainfall event
            self.T_steps = [x+self.add_dry_timesteps for x in self.T_steps]
            dry_wd = torch.zeros((add_dry_timesteps, self.px, self.py))
            dry_hydrograph = torch.zeros(add_dry_timesteps)
            for i in range(len(self.waterdepth)):
                self.waterdepth[i] = torch.cat((dry_wd, self.waterdepth[i]), dim=0)
                self.rainfall_events[i] = torch.cat((dry_hydrograph, self.rainfall_events[i]), dim=0)

        self.sample_type = sample_type
        if self.sample_type == "full":
            self.fix_indexes = False
        else:
            self.fix_indexes = fix_indexes

        self.precision = precision
        self.tau = tau
        self.upsilon = upsilon
        self.nx = dim_patch
        self.ny = dim_patch
        self.curr_index = 0
        self.x_p = 0
        self.y_p = 0
        self.build_indexes()

        assert(len(self.rainfall_events)==self.N_events)
        for i in range(self.N_events):
            assert(self.rainfall_events[i].shape[0]==self.waterdepth[i].shape[0]==self.T_steps[i])
        assert(self.dem.shape[0]==self.px)
        assert(self.dem.shape[1]==self.py)        
        assert(self.dem_mask.shape[0]==self.px)
        assert(self.dem_mask.shape[1]==self.py)
        if not(self.sample_type == "full"): 
            assert(self.nx<=self.px)
            assert(self.ny<=self.py)
        
    def pad_borders(self, x, value):
        return pad(x, 4*[self.border_size], mode='constant', value=value)
    
    def build_indexes(self):
        '''
        Builds indexes for timesteps, events to be returned
        '''
        self.indexes_t = []
        self.indexes_e = []
        v = 0
        for i, nt in enumerate(self.T_steps):
            nv = nt - self.lead_time
            if self.timestep:
                nv = nv - (self.timestep)
            if self.ts_out:
                nv = nv - (self.ts_out-1) 
            if self.convlstm:
                nv = nv - (self.convlstm -1)
            if nv>0:
                self.indexes_t.append(np.arange(nv))
                self.indexes_e.append(np.ones([nv], dtype=int)*i)
                v += nv
        self.indexes_t = np.concatenate(self.indexes_t)
        self.indexes_e = np.concatenate(self.indexes_e)
        self.N_batch_images = len(self.indexes_t)
        if self.fix_indexes:
            self.inds = self.get_all_fix_indexes()   # returns all possible incexes without random generation
        else:
            self.inds = 0                  # cannot return None        

            
    def __exit__(self, *args):
        self.h5file.close()

    def __getitem__(self, index):
        '''
        Generates one sample of data by accessing event (index_e), 
        timestep for each file (index_t) and indexes (index_s) for each file 
        index_s refers to the patch 
        
        if fix_indexes = True, data is generated sequentially
        
        '''
        # Select sample

        if self.fix_indexes:    ###   
            tmp = index // len(self.inds)
            index_e = self.indexes_e[tmp] 
            index_t = self.indexes_t[tmp]
            index_s = index % len(self.inds)  
            
            self.curr_index = index_s
            self.x_p, self.y_p = self.inds[index_s]
            
        else:     
            index_e = self.indexes_e[index] 
            index_t = self.indexes_t[index]
            index_s = 0
        
        # select input data. based on current index_e, index_t and number of timesteps
        if self.convlstm is None:     
            xin = self.waterdepth[index_e][index_t: index_t + self.timestep]
            if self.ts_out:
                xout = self.waterdepth[index_e][index_t + self.timestep + self.lead_time: index_t + self.timestep + self.lead_time + self.ts_out]
            else:
                xout = self.waterdepth[index_e][index_t + self.timestep + self.lead_time]
        # select input data for convlstm case
        else:
            xin = self.waterdepth[index_e][index_t: index_t + self.convlstm]
            xout = self.waterdepth[index_e][index_t + self.convlstm + self.lead_time: index_t + self.convlstm + self.lead_time + self.ts_out]
        
        # constant inputs
        mask = self.dem_mask
        dem = self.dem
        diff_dem = self.diff_dem
        topo_index = self.topo_index

        if self.sample_type == "single":

            if self.fix_indexes:
                x_p, y_p = self.inds[index_s]    # finds adjacent coordinates
            else:
                x_p, y_p = self.find_patch(mask, xin, index_t)
            xin, mask, dem, diff_dem, topo_index, xout = self.crop_to_patch(x_p, y_p, xin, mask, dem, diff_dem, topo_index, xout)

        y = self.build_output(xout, mask)
        
        if self.convlstm is None:
            x, mask = self.build_inputs(xin, self.rainfall_events[index_e][index_t:index_t+ max(self.lead_time,self.timestep)], mask, dem, diff_dem, topo_index)
        else:
            x, mask = self.build_inputs_convlstm(xin, self.rainfall_events[index_e][index_t:index_t+ self.convlstm+self.lead_time], mask, dem, diff_dem, topo_index)
        
        if self.sample_type == "single":
            return {'x': x,'y':  y, 'mask': mask, 'index_e':index_e, 'timestep':index_t, 'index_s':index_s, 'x_p': x_p, 'y_p':y_p} 
        else:
            return {'x': x,'y':  y, 'mask': mask, 'index_e':index_e, 'timestep':index_t, 'index_s':index_s} 
    
    
    def crop_to_patch(self, x_p, y_p, xin, mask, dem, diff_dem, topo_index, xout=None):
        '''
        Crops a patch of xin, mask, dem, xout based on current coordinates x_p, y_p
        '''
        xin = xin[:, x_p:x_p+self.nx, y_p:y_p+self.ny]
        diff_dem = diff_dem[:, x_p:x_p+self.nx, y_p:y_p+self.ny]
        topo_index = topo_index[x_p:x_p+self.nx, y_p:y_p+self.ny]
        mask = mask[x_p:x_p+self.nx, y_p:y_p+self.ny]
        dem = dem[x_p:x_p+self.nx, y_p:y_p+self.ny]
        if xout is not None and self.ts_out:
            xout = xout[:, x_p:x_p+self.nx, y_p:y_p+self.ny]
        elif xout is not None:
            xout = xout[x_p:x_p+self.nx, y_p:y_p+self.ny]
        return xin, mask, dem, diff_dem, topo_index, xout
    
    def build_output(self, xout, mask):
        '''
        returns ground truth
        '''
        y = xout

        if self.border_size:
            border = self.border_size
            y = y[:, border:-border, border:-border]
            mask = mask[border:-border, border:-border]
        return y

    def build_inputs_convlstm(self, xin, rainfall_events, mask, dem, diff_dem, topo_index):
        '''
        Builds input channels for convlstm and returns (inputs, mask)
        Use self.convlstm to define the number of timesteps as convlstm input
        Channels are: dem, rainfall, wd@(timestep-1), diff_dem, topo_index
        '''
        # create channels for rainfall
        rain_t = torch.zeros((self.convlstm + self.lead_time -1), *dem.shape)

        # creates channels for rainfall
        for i in range(self.convlstm + self.lead_time -1):    
            rain_t[i, mask] = rainfall_events[i] 
  
        # one sample for ConvLSTM is 4D with the number of timesteps as additional dimension
        x = torch.zeros((self.input_channels, self.convlstm, *dem.shape))
        x[0] = dem.clone()
        if self.use_diff_dem:
            x[1:5] = diff_dem
        if self.use_topo_index:
            x[1+(4 if self.use_diff_dem else 0)] = topo_index

        # rainfall channels
        for i in range(self.convlstm):
            x[1+self.n_topo_channels: 
                1+self.n_topo_channels+self.n_rain_channels, i] = rain_t[i:i+self.n_rain_channels]
        
        # waterdepth channels
        x[1+self.n_topo_channels+self.n_rain_channels: 
            1+self.n_topo_channels+self.n_rain_channels+self.timestep] = xin
        
        mask = mask.unsqueeze(0).clone()

        # depending on model precision choose
        if self.precision == 16:
            x = x.half()
            mask = mask.half()
        if self.precision == 64:
            x = x.double()
            mask = mask.double()
        return x, mask
    
    def build_inputs(self, xin, rainfall_events, mask, dem, diff_dem, topo_index):

        '''
        Builds input channels for the model and returns (inputs, mask)
        Channels are: dem, rainfall, wd@(timestep-1), diff_dem, topo_index
        
        Optional: if timestep>2, the number of channels is increased accordingly and normalized
        '''
        rain_t = torch.zeros((max(self.timestep, self.lead_time), *dem.shape))

        # creates channels for rainfall
        for i in range(max(self.timestep, self.lead_time)):    
            rain_t[i, mask] = rainfall_events[i] 

        if self.accumulate_rain:
            rain_t = torch.sum(rain_t, 0)    

        x = torch.zeros((self.input_channels, *dem.shape))
        x[0] = dem.clone()
        if self.use_diff_dem:
            x[1:5] = diff_dem
        if self.use_topo_index:
            x[1+(4 if self.use_diff_dem else 0)] = topo_index
        # rainfall channels
        x[1+self.n_topo_channels : 1+self.n_topo_channels+self.n_rain_channels] = rain_t
        # waterdepth channels
        x[1+self.n_topo_channels+self.n_rain_channels : 1+self.n_topo_channels+self.n_rain_channels+self.timestep] = xin        

        mask = mask.unsqueeze(0).clone()
        return x, mask

    def find_patch(self, mask, xin, index_t):
        '''
        create patches with:
        - at least a fraction of tau non-zero elements in mask AND
        - at least an average water depth value within those elements larger than upsilon (in m) 
        '''
        
        if self.random_patches:
            
            z_patch = torch.zeros(self.nx, self.ny)
            xin_patch = torch.zeros(self.nx, self.ny)
            border = self.border_size if self.do_pad else 0
            bool_wd = True


            while ((torch.sum(z_patch) < (self.nx*self.ny * self.tau)) or bool_wd): 

                x_p = np.random.randint(0, self.px-self.nx+1)
                y_p = np.random.randint(0, self.py-self.ny+1)

                xin_patch = xin[:, x_p:(x_p + self.nx+border*2), y_p:(y_p + self.ny+border*2)]  

                # compute z_patch and bool_wd for while statement
                z_patch = mask[x_p:(x_p + self.nx+border*2), y_p:(y_p + self.ny+border*2)]
                if (index_t == 0 or index_t ==1 or index_t ==2): # check if we are at timestep zero, where WD out is usually all zero
                    bool_wd = False  # stops the while loop
                else:
                    bool_wd = (torch.sum(xin_patch)/torch.sum(mask)) < self.upsilon
        else: 
            x_p = (self.px - self.nx) // 2
            y_p = (self.py - self.ny) // 2
            
        return x_p, y_p

    def get_all_fix_indexes(self, non_full=False):
        '''
        use this function if every patch is padded with border = border_size
        finds indeces to create adjacent overlapping patches
        the patches need to move steps = nx - border_size*2 to guarantee complete reconstruction of the catchment
        nx = ny = dim_patch
        px = waterdepth.shape[0]
        py = waterdepth.shape[1]
        '''
        border = self.border_size if self.do_pad else 0
        if non_full:
            mx = self.px - 2*border 
            my = self.py - 2*border 
        else:
            mx = self.px - self.nx + 1 
            my = self.py - self.ny + 1
        # use overlapping patches if test dataset
        if "test" in self.h5file.filename: 
            indx =  np.arange(0, mx, int(self.nx/2))
            indy =  np.arange(0, my, int(self.ny/2))    
        else:
            indx =  np.arange(0, mx, self.nx-border*2)    
            indy = np.arange(0, my, self.ny-border*2)         
        x, y = np.meshgrid(indx,indy)
        return np.array([x.flatten(), y.flatten()]).T    
        
    
    def __len__(self):
        'Denotes the total number of samples'
        if self.fix_indexes:
            return self.N_batch_images * len(self.inds)
        else:
            return self.N_batch_images
