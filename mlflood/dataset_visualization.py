import matplotlib.pyplot as plt
from numpy import NaN

"""
import matplotlib
matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
    "font.size":8
})
"""

def visualize_batch(batch, use_diff_dem, use_topo_index):
    # define input and target
    input, target = batch['x'], batch['y']

    # define figure size
    nrows, ncols = input.shape[1], input.shape[0]
    fig, axs = plt.subplots(nrows+1, ncols)
    plot_width = 30
    plot_length = nrows/ncols*plot_width
    plt.rcParams['figure.figsize'] = [plot_width, plot_length]

    # color map depending on options
    if use_diff_dem:
        cmaps = ['copper', 'copper', 'copper', 'copper', 'copper','viridis', 'RdBu_r', 'RdBu_r']
    if not(use_diff_dem):
        cmaps = ['copper', 'viridis', 'RdBu_r', 'RdBu_r']
    if use_topo_index:
        cmaps = ['copper', 'copper', 'viridis', 'RdBu_r', 'RdBu_r']

    # plot input
    for col in range(ncols):
        for row in range(nrows):
            img = input[col,row,:,:]
            img[img < 0] = NaN
            ax = axs[row, col]
            im = ax.imshow(img, cmap = cmaps[row])
            fig.colorbar(im, ax = ax)

    # plot target
    for col in range(ncols):
        img = target[col,0,:,:]
        ax = axs[nrows, col]
        im = ax.imshow(img, cmap = cmaps[nrows])
        fig.colorbar(im, ax = ax)

    plt.show()

def visualize_sample(sample, use_diff_dem, use_topo_index):
    # define input and target
    input, target = sample['x'], sample['y']

    # define figure size
    fig, axs = plt.subplots(ncols=5)#input.shape[0]+1)
    plot_width = 12
    plot_length = 0.3*plot_width
    plt.rcParams['figure.figsize'] = [plot_width, plot_length]

    # color map depending on options
    # color map depending on options
    if use_diff_dem:
        cmaps = ['copper', 'copper', 'copper', 'copper', 'copper','viridis', 'RdBu_r', 'RdBu_r']
        titles = ['DEM', 'D1', 'D2', 'D3', 'D4', 'rainfall', 'WD', 'WD target']
    if not(use_diff_dem):
        cmaps = ['copper', 'viridis', 'RdBu_r', 'RdBu_r']
        titles = ['DEM', 'rainfall', 'WD', 'WD target']
    if use_topo_index:
        cmaps = ['copper', 'copper', 'viridis', 'RdBu_r', 'RdBu_r']
        titles = ['elevation', 'topographic index', 'rainfall: t=55', 'water depth: t=55', 'target water depth: t=60']

    # create an axes on the right side of ax. The width of cax will be 5%
    # of ax and the padding between cax and ax will be fixed at 0.05 inch.
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    # plot input
    for col in range(4):
        img = input[col,:,:]
        ax = axs[col]
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        im = ax.imshow(img, cmap = cmaps[col])
        fig.colorbar(im, cax = cax)
        ax.set_title(titles[col], fontsize=8)
        if col>0:
            ax.set_yticklabels([])

    # plot target
    col = 4#input.shape[0]
    img = target[0,:,:]
    ax = axs[col]
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    im = ax.imshow(img, cmap = cmaps[col])
    fig.colorbar(im, cax = cax) 
    ax.set_title(titles[col], fontsize=8)
    ax.set_yticklabels([])

    #plt.suptitle("Patch: " + str(sample['index_s']) + ', Timestep: '+ str(sample['timestep']) + ', Event:' + str(sample['index_e']), fontsize=36, weight='bold')      
    plt.show()
    plt.tight_layout()

    fig.savefig("/scratch/tdonauer/tabea_mt/plots/training_data/sample.pdf", bbox_inches = 'tight', dpi=3000, pad_inches = 0)
    #fig.savefig("/scratch/tdonauer/tabea_mt/plots/training_data/sample.pgf", bbox_inches = 'tight', pad_inches = 0)
    #fig.savefig("/scratch/tdonauer/tabea_mt/plots/training_data/sample.jpg", dpi=3000)