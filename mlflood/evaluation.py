from concurrent.futures import thread
from turtle import color
import numpy as np
from tqdm import tqdm
import torch
import matplotlib.ticker as ticker
from mlflood.dataset import unnormalize_waterdepth
from sklearn.metrics import r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from typing import Optional, List, Tuple
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import mark_inset, inset_axes, TransformedBbox, BboxPatch, BboxConnector
from mpl_toolkits.axes_grid1 import make_axes_locatable
make_axes_locatable
import re

# plot settings

"""
plt.rcParams.update(plt.rcParamsDefault)    
plt.rcParams.update({
        "text.usetex": False,
        "font.size":12
        })
"""
plt.rcParams.update({
        "text.usetex": True,
        "font.family": "sans.serif",
        "font.sans-serif": 'Helvetica',
        "font.size":12
        })
        

# specify device
cuda_device = 0
device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")

# utils
def my_mark_inset(parent_axes, inset_axes, loc1a=1, loc1b=1, loc2a=2, loc2b=2, **kwargs):
    rect = TransformedBbox(inset_axes.viewLim, parent_axes.transData)
    pp = BboxPatch(rect, fill=False, **kwargs)
    parent_axes.add_patch(pp)
    p1 = BboxConnector(inset_axes.bbox, rect, loc1=loc1a, loc2=loc1b, **kwargs)
    inset_axes.add_patch(p1)
    p1.set_clip_on(False)
    p2 = BboxConnector(inset_axes.bbox, rect, loc1=loc2a, loc2=loc2b, **kwargs)
    inset_axes.add_patch(p2)
    p2.set_clip_on(False)

    return pp, p1, p2

############################################################################################################################################################
##################################################################  Data Visualization ############################################################################ 


def plot_pie(wd, lims, labels):
    '''
    Pie plot of waterdepth values at a specific timestep
    
    Inputs
    wd: tensor of waterdepth values at a specific timestep
    lims: tuple of values we want to split the data into
    labels: labels for the plot
    '''
    
    if len(labels) == 5:
        sizes = [torch.sum(wd<lims[0]), 
                 torch.sum((wd>lims[0]) & (wd<lims[1])), 
                 torch.sum((wd>lims[1]) & (wd<lims[2])), 
                 torch.sum((wd>lims[2]) & (wd<lims[3])), 
                 torch.sum(wd>lims[3])]
    else:
        sizes = [
                 torch.sum((wd>lims[0]) & (wd<lims[1])), 
                 torch.sum((wd>lims[1]) & (wd<lims[2])), 
                 torch.sum((wd>lims[2]) & (wd<lims[3])), 
                 torch.sum(wd>lims[3])]
    fig1, ax1 = plt.subplots(figsize = [18,6])
    ax1.pie(sizes, labels=labels, autopct='%1.1f%%',
            shadow=True, startangle=90)
    ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.

    plt.show()
    
    
# Waterdepth evolution
def plot_wd_evol(wd_time, lims, labels):
    '''
    plots evolution over time of waterdepth values
    
    Inputs
    wd_time: tensor of waterdepth values
    lims: tuple of values we want to split the data into
    labels: labels for the plot
    '''
    import pandas as pd

    wd_evolution = []
    for i in range(len(wd_time)):
        wd = wd_time [i]
        
        if len(labels) == 5:
            sizes = [torch.sum(wd<lims[0]), 
                     torch.sum((wd>lims[0]) & (wd<lims[1])), 
                     torch.sum((wd>lims[1]) & (wd<lims[2])), 
                     torch.sum((wd>lims[2]) & (wd<lims[3])), 
                     torch.sum(wd>lims[3])]
            sizes = sizes/(np.sum(sizes))
            wd_evolution.append(sizes)
        else:
            sizes = [ 
                     torch.sum((wd>lims[0]) & (wd<lims[1])), 
                     torch.sum((wd>lims[1]) & (wd<lims[2])), 
                     torch.sum((wd>lims[2]) & (wd<lims[3])), 
                     torch.sum(wd>lims[3])]
            sizes = sizes/(np.sum(sizes))
            wd_evolution.append(sizes)   
            
    a_df = pd.DataFrame(data=wd_evolution, columns=labels)
    
    fig, ax = plt.subplots(figsize = [8,6])
    for j in range(len(labels)):
        plt.plot(a_df[labels[j]], label = labels[j])
    plt.legend()
    plt.ylabel('Normalized waterdepth')
    plt.xlabel('Timesteps')
    plt.title('Evolution of Water Depth')
    plt.show()
    
    
############################################################################################################################################################
##################################################################  MAE and MSE  ############################################################################ 


def mse(p):
    """Compute MSE"""
    mses = []
    for v1, v2, mask in p:
        mses.append(np.mean((v1[:,mask]-v2[:,mask])**2, axis=1))
    return mses

def mses2plot(mses):
    """Transforms the compute MSEs into curves for plots"""
    tmax = max([len(e) for e in mses])
    n_mses = np.zeros([tmax])
    m_mses = np.zeros([tmax])
    s_mses = [[] for _ in range(tmax)]
    for i, e in enumerate(mses):
        t = len(e)
        m_mses[:t] += e
        n_mses[:t] += 1
        for j in range(t):
            s_mses[j].append(e[j])
    m_mses = m_mses/n_mses
    s_mses = np.array([np.std(np.array(e)) for e in  s_mses])
    s_mses = s_mses/np.sqrt(n_mses)
    return m_mses, s_mses
    
def proportional_threshold_event(gd: np.array, mask: np.array, percentile=99):
    # mask pixels outside the catchment
    if mask is not None:
        gd = gd[:, mask]
    # compute water depth at the given percentile
    gd_upper = np.percentile(gd, percentile, axis=(1,2))
    return gd_upper

def mse_from_predicted_dataset(predictions_ag):
    """Given the prediction for a dataset, compute the MSE for each event and each timestep."""
    mse = []
    for event in predictions_ag:
        mse_event = []
        for y, y_pred, mask in zip(*event):
            mse_event.append(np.mean((y[:,mask]-y_pred[:,mask])**2))
        mse.append(mse_event)
    return mse

def mae_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, wd_threshold=0.05) -> np.array:
    """MAE for an event.
    
    Return a 1D array of MAE corresponding ot each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]

    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    return np.nanmean(np.abs(pred-gd), axis=1)

def ae_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, wd_threshold=0.05) -> np.array:
    """Sum of Absolute error for an event.
    
    Return a 1D array of SAE corresponding ot each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]

    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    return np.nansum(np.abs(pred-gd), axis=1)

def mape_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, wd_threshold=0.05) -> np.array:
    """MAPE for an event.
    
    Return a 1D array of MAPE corresponding to each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]
    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)

    return np.nanmean(np.array(abs(pred-gd)/gd), axis=1)

def nse_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, wd_threshold=0.05) -> np.array:
    """Nash-Sutcliff-Efficiency for an event.
    
    Return a 1D array of NSE at each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]

    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)

    # compute per pixel temporal mean
    gd_temporal_mean = np.nanmean(gd, axis=0)
    return (1- np.nansum((pred-gd)**2, axis=1)/np.nansum((gd-gd_temporal_mean)**2, axis=1))

def precision_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, thr=0.5, wd_threshold=0.05) -> np.array:
    """Precision for an event
    Return 1D array of F1score (thresholds 0.05 and 0.5 m) at each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]

    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    TP = np.count_nonzero((gd>thr[:,None]) & (pred>thr[:,None]), axis=1)
    FP = np.count_nonzero((gd<=thr[:,None]) & (pred>thr[:,None]), axis=1)
    return (TP/(TP+FP))


def recall_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, thr=0.5, wd_threshold=0.05) -> np.array:
    """Recall for an event
    Return 1D array of CSI (thresholds 0.05 and 0.5 m) at each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]
    
    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    
    TP = np.count_nonzero((gd>thr[:,None]) & (pred>thr[:,None]), axis=1)
    FN = np.count_nonzero((gd>thr[:,None]) & (pred<=thr[:,None]), axis=1)
    return (TP/(TP+FN))

def mse_event(pred: np.array, gd: np.array, mask: Optional[np.array]=None, wd_threshold=0.05) -> np.array:
    """MSE for an event.
    
    Return a 1D array of MSE corresponding ot each timestep.
    """
    if mask is not None:
        pred = pred[:, mask]
        gd = gd[:, mask]
    
    # additionally mask water depths below 0.05 m in the gd (according to Lowe et al)
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    return np.mean((pred-gd)**2, axis=1)


def split_values(pred: np.array, gd: np.array, lims: tuple=(0.2, 1), wd_threshold=0.05) -> List[Tuple[np.array, np.array]]:
    """Split the value of pred and gd according to lims. 
    
    This function should be applied after the application of the mask.
    
    Return a list of splits: [(pred 1d array, gd 1d array), ...]
    """
    vmin = min(np.nanmin(np.array(gd)), np.nanmin(np.array(pred)))
    if vmin < 0:
        print(ValueError(f"Negative waterdept value: {vmin}"))
    vmax = np.nanmax(np.array(gd))*2
    if vmax < lims[-1]:
        print("Some bins are empty")
    
    lims = [vmin, *lims, vmax]
    splits = []
    for vmin, vmax in zip(lims[:-1], lims[1:]):
        selection = np.logical_and(gd>=vmin, gd<vmax)
        # selection = selection.type(torch.BoolTensor)
        splits.append((pred[selection], gd[selection]))
    # assert sum([len(split[0]) for split in splits]) in [len(gd), gd.size, gd.numel()]
    # for lead_time models use len(gd)) else gd.size
    
    return splits

def boxplot_metric(pred, gd, mask, lims: tuple=(0.2, 1), pred_ts = None, metric="mae", wd_threshold=0.05):
    """
    Compute the mae for a boxplot
    pred_ts indicates how many timesteps ahead we are looking at
    """
    if pred_ts:
        pred = pred[pred_ts, mask]
        gd = gd[pred_ts, mask]
    else:
        pred = pred[:, mask]
        gd = gd[:, mask]
    
    # only consider pixels where either gt or pred is higher than 0.05 m
    wd_mask = np.logical_or(np.greater_equal(pred, wd_threshold), np.greater_equal(gd, wd_threshold))
    gd = np.where(wd_mask, gd, np.nan)
    pred = np.where(wd_mask, pred, np.nan)
    splits = split_values(pred, gd, lims=lims)
    if metric=="mae":
        boxplot = np.array([np.abs(split[0]-split[1]).flatten() for split in splits],dtype=object)
    elif metric=="mse":
        boxplot = np.array([((split[0]-split[1])**2).flatten() for split in splits], dtype=object)
    return boxplot


############################################################################################################################################################
##################################################################  Predictions ############################################################################ 

def predict_batch(model, dataset):
    """MSE 1 step for all batches."""
    outputs = []
    
    with torch.no_grad():  
        for sample in tqdm(dataset):

            x = sample['x'].to(device)
            mask = sample['mask'].to(device)
            y = sample['y'].to(device)
            predictions = model(x, mask) 
            y = torch.mul(y, mask)

            y_pred = predictions['y_pred']
            mask = predictions['mask']
            y_pred = torch.mul(y_pred, mask)

            y = y.detach().cpu().numpy()[0]
            y_pred = y_pred.detach().cpu().numpy()[0]
                
            outputs.append((y_pred, y))
        return outputs

def predict_dataset(model, dataset, start_ts=None, ar = True):
    """Predict all events in a dataset."""

    predictions_ag = []

    for i in tqdm(range(dataset.N_events)):
        predictions_ag.append(predict_event(model, dataset, i, start_ts=start_ts, ar = ar))
    return  predictions_ag

def crop_overlapping_patches(y_pred, patch_dim):
    '''
    crops a patch to fit it for the overlapping reconstruction
    '''
    cropped = y_pred[10: int(patch_dim/2) + 10, 
                    10:int(patch_dim/2) + 10]
    return cropped

def predict_event(model, dataset, event_num, start_ts=None, ar = True, ts_out = None, convlstm=None):
    """Predict a full event.
    Predict a full event using overlapping patches. 
    The fuction returns a reconstructed catchment minus a border size of 10 to remove border effect between patches.

    This function will split the prediction if `dataset.sample_type == "single"`. 
    Alternatively, it will use the full frame to predict if `dataset.sample_type == "full"`.
    
    ar: auto regressively
    start_ts: start at timestep start_ts (if None starts at 0.)
    """

    patch_dim = dataset.nx
    b = dataset.border_size
    timestep = dataset.timestep
    if start_ts is None:
        start_ts = timestep-1
    assert start_ts >= timestep-1
    T = len(dataset.rainfall_events[event_num]) - start_ts - dataset.lead_time 
    index_t = start_ts - timestep + 1

    if convlstm is not None:
        start_ts = convlstm - 1
        T = len(dataset.rainfall_events[event_num]) - start_ts - dataset.lead_time
        index_t = start_ts - convlstm + 1

    if ar:
        # change this according to how many timesteps ahead you want to predict in ar mode
        T = 6

    # model inputs
    xin = dataset.waterdepth[event_num][index_t : index_t + timestep]
    rainfall = dataset.rainfall_events[event_num][index_t : ]
    dem = dataset.dem
    diff_dem = dataset.diff_dem
    topo_index = dataset.topo_index
    mask = dataset.dem_mask
    recons_pred_full = torch.zeros(T, dataset.px - 2*b, dataset.py - 2*b)    

    if convlstm is not None:
        xin = dataset.waterdepth[event_num][index_t : index_t + convlstm]

    for t in tqdm(range(T)):
        if convlstm is None:
            recons_pred_full[t] = predict_next_ts(dataset, model, xin, rainfall[t:t+max(dataset.lead_time,dataset.timestep)], 
            mask, dem, diff_dem, topo_index, ts_out, convlstm) 
        else:
            recons_pred_full[t] = predict_next_ts(dataset, model, xin, rainfall[t:t+convlstm+dataset.lead_time-1], 
            mask, dem, diff_dem, topo_index, ts_out, convlstm) 

        if ar:
            if (timestep>1) or (convlstm is not None): # multiple WD maps as inputs, copy previous timesteps
                xin[:-1] = xin[1:].clone()
            xin[-1] = dataset.pad_borders(recons_pred_full[t].clone(), 0) # use model prediction for current WD input
        else:
            xin = dataset.waterdepth[event_num][index_t+t+1: index_t+t+timestep+1].clone()
        
        if convlstm is not None:
            xin = dataset.waterdepth[event_num][index_t+t+1: index_t+t+1+convlstm]
  
    if b:
        recons_gt_full = dataset.waterdepth[event_num][-T :, b : -b, b: -b]
        recons_mask_full = mask[b : -b, b : -b ]
    else:
        recons_gt_full = dataset.waterdepth[event_num][-T :, : , :]
        recons_mask_full = mask
    
    # unnormalize waterdepth maps
    max_wd = dataset.max_wd
    recons_pred_full = unnormalize_waterdepth(recons_pred_full, max_wd)
    recons_gt_full = unnormalize_waterdepth(recons_gt_full, max_wd)
    return recons_pred_full.numpy(), recons_gt_full.numpy(), recons_mask_full.numpy()


def predict_next_ts(dataset, model, xin, rainfall, mask, dem, diff_dem, topo_index, ts_out, convlstm) :
    """Predict the next timestep."""

    b = dataset.border_size
        
    if dataset.sample_type == "full":             ### check
        if convlstm is None:
            x_in, mask_in = dataset.build_inputs(xin, rainfall, mask, dem, diff_dem, topo_index)
        else:
            x_in, mask_in = dataset.build_inputs_convlstm(xin, rainfall, mask, dem, diff_dem, topo_index)

        x_in = x_in.unsqueeze(0).to(device)
        mask_in = mask_in.unsqueeze(0).to(device)
        
        # model prediction
        predictions = model(x_in, mask_in)
        y_pred = predictions['y_pred'].detach().cpu()
        recons_pred = y_pred

    else:
        nx = dataset.nx
        ny = dataset.ny

        inds = dataset.get_all_fix_indexes(non_full=False) 
        recons_pred = torch.zeros(dataset.px - 2*b, dataset.py - 2*b)   

        for inds_count, (x_p, y_p) in enumerate(inds):
            # crop patches, build model inputs
            xin_crop, mask_crop, dem_crop, diff_dem_crop, topo_index_crop, _ = dataset.crop_to_patch(x_p, y_p, xin, mask, dem, diff_dem, topo_index)                

            if convlstm is None:
                x_in, mask_in = dataset.build_inputs(xin_crop, rainfall, mask_crop, dem_crop, diff_dem_crop, topo_index_crop)

            else:
                x_in, mask_in = dataset.build_inputs_convlstm(xin_crop, rainfall, mask_crop, dem_crop, diff_dem_crop, topo_index_crop)

            # move input to GPU
            x_in = x_in.unsqueeze(0).to(device)
            mask_in = mask_in.unsqueeze(0).to(device)
            
            # model prediction
            predictions = model(x_in, mask_in)
            y_pred = predictions['y_pred'].squeeze().detach().cpu()
            
            if "test" in dataset.h5file:
                recons_pred[x_p + 10:
                     x_p + 10 + (int(dataset.patch_dim/2)),
                     y_p + 10:
                     y_p + 10 + (int(dataset.patch_dim/2))] = crop_overlapping_patches(y_pred, dataset.patch_dim)

            else:
                x_p2 = x_p + (nx-b*2) 
                y_p2 = y_p + (ny-b*2)
                recons_pred[x_p:x_p2, y_p:y_p2] = y_pred
    
    return recons_pred




############################################################################################################################################################
##################################################################  Plots ############################################################################ 

#################  Quantitative plots ##################################

def plot_metric_event(metric, labels, colors, styles, start_ts=0, metric_label = 'Mean absolute error [cm]', save_folder = None, rainfall_series=None, name = 'autoregressive', title = None):
    '''
    Plot metric over time for different models. This function is valid for both 1timestep ahead and autoregressive mode
    Inputs: 
        - array of MAEs (each computed with mae_event()) or mse_event
        - array of labels
    '''  
    nt = len(metric[0])
    # t = start_ts + 1 + np.arange(nt)        # should start from start_ts + 1
    t = np.arange(nt)*5
    fig, ax1 = plt.subplots(figsize=(3,9/4))

    for i, (metric, label) in enumerate(zip(metric, labels)):
        ax1.plot(t, metric*100, label=label, color=colors[i], linestyle=styles[i], linewidth=0.7)   
    fig.tight_layout()
    ax1.legend(loc=0, fontsize=6)
    #ax1.set_ylabel(metric_label,  fontsize=6)
    ax1.set_xlabel('Time [min]',  fontsize=6)
    ax1.set_xlim(0, 5*(len(t)+1))
    ax1.set_ylim(0,2.2e7)
    #ax1.set_ylim(0, 12)
    #ax1.set_xticks(np.arange(0,len(t),5), fontsize=18)
    #ax1.set_yticks(np.arange(0,14,2))

    if rainfall_series is not None:
        ax2 = ax1.twinx()
        #ax2.bar(t,rainfall_series[:nt], color='#104E8B', width=4.5)
        #ax2.spines['right'].set_color('#104E8B')
        #ax2.tick_params(colors='#104E8B', which='both')
        #ax2.yaxis.label.set_color('#104E8B')
        ax2.set_ylabel('Rainfall intensity [mm/h]',  fontsize=6)
        ax2.bar(t,rainfall_series, color='#2167bf', width=4)
        ax2.spines['right'].set_color('#2167bf')
        ax2.tick_params(colors='#2167bf', which='both')
        ax2.yaxis.label.set_color('#2167bf')
        ax2.set_ylim(0,450)
        ax2.set_yticks(np.arange(0,450,100))
        #ax2.set_ylim(0,3*np.max(np.array(rainfall_series)))
        #ax2.set_yticks(np.arange(0, 3*np.max(np.array(rainfall_series))), fontsize=18)
        #ax2.set_ylim(0,9)
        #ax2.legend(['Rainfall'], loc=4, fontsize=16)
        plt.gca().invert_yaxis()
    
    if title:
        ax1.set_title(title, fontsize = 18 ,fontweight="bold")
    plt.show()
    if save_folder:
        fig.tight_layout()
        filename = save_folder  + name +'.jpg'
        fig.savefig(filename, dpi=1200)#, bbox_inches='tight') 
        filename = save_folder  + name +'.pgf'
        fig.savefig(filename, dpi=1200)#, bbox_inches='tight')     
    
    
def multiboxplot(data, ticks, labels, colors, save_folder = None, metric_label = 'Absolute error [cm]', name = 'autoregressive', title = None):
    
    '''
    Plot Boxplots for multiple models. This function is valid for both 1timestep ahead and autoregressive mode
    Inputs: 
        - data: array of data (each computed with boxplot_mae())
        - array of xticks of lenght 5
        - array of labels of len(data)
        - array of colors of lenght 5
        
    '''  
        
    def set_box_color(bp, color):
        plt.setp(bp['boxes'], color=color)
        plt.setp(bp['whiskers'], color=color)
        plt.setp(bp['caps'], color=color)
        plt.setp(bp['medians'], color="black")
    
    assert len(data)==len(labels)
    
    nbox = len(data)
    # adjust width
    plt.figure(figsize=(nbox,2.5))
    flierprops = dict(marker='d', markerfacecolor='black', markersize=4, linestyle='none', markeredgecolor='black')
    for i, (d, label, c) in enumerate(zip(data, labels, colors)):
        nticks = len(d)
        s = 1.5/(4*nticks+3)
        #  assert nticks == len(d)
        bpl = plt.boxplot(d*100, positions=np.array(range(nticks))*2.0-2*s+4*s*i, notch=True, patch_artist=True, showfliers = True, flierprops=flierprops, sym='', widths=3*s)
        set_box_color(bpl, c) 

        # draw temporary red and blue lines and use them to create a legend
        plt.plot([], c=c, label=label)
    plt.legend(fontsize=8)
    plt.ylabel(metric_label, fontsize=8)
    plt.xticks(range(0, len(ticks) * 2, 2), ticks)
    plt.xlim(-1, len(ticks)*2)
    plt.ylim(-1, 46)
    plt.tight_layout()
    if title:
        plt.title(title, fontsize = 8 ,fontweight="bold")
    
    if save_folder:
        filename = save_folder + 'Multiboxplots_' + name +'.jpg'
        plt.savefig(filename, dpi=1200, bbox_inches = 'tight')
        filename = save_folder + 'Multiboxplots_' + name +'.pgf'
        plt.savefig(filename, dpi=1200, bbox_inches = 'tight')
    #plt.show()
    
#################  Qualitative plots ##################################    

def plot_answer_sample(pred, gt, mask, ts, zoom=None, show_diff=False, global_scale=False, save_folder = None, model_name = 'model', start_ts=None, lead_time=None, ar=False):
    
    '''
    Visualize a full or zoomed in reconstruction for 1 timestep, for a single model. This function is valid for both 1timestep ahead and autoregressive mode
    Inputs: 
        - predictions
        - ground truth
        - ts: how many timesteps ahead of our starting prediciton time we are looking at
        - mask
        - zoom: array of coordinates  
    '''  
        
    import matplotlib.colors as colors    
    cmin = 0.01
    cmap = "hot_r"
    cmap = "gist_heat_r"
    #     cmap='seismic'
    pred = np.where(mask, pred, np.nan)
    gt = np.where(mask, gt, np.nan)

    if ts is not None:
        pred = pred[ts]
        gt = gt[ts]

    if show_diff:
        fig, axs = plt.subplots(2,3, figsize=(18,10))
        axs = [axs[0][0], axs[0][1], axs[0][2], axs[1][0], axs[1][1], axs[1][2]]
    else:
        fig, axs = plt.subplots(1,3, figsize=(16,5))

    """
    if zoom is not None:
        pred = pred[zoom[0]:zoom[1], zoom[2]:zoom[3]]
        gt = gt[zoom[0]:zoom[1], zoom[2]:zoom[3]]
        mask = mask[zoom[0]:zoom[1], zoom[2]:zoom[3]]  
        """  
    #diff = np.abs(gt - pred)
    diff = pred - gt

    if global_scale:
        vmax_abs = max(np.nanmax(np.array(gt)), cmin)
        vmax_diff = max(np.nanmax(np.abs(np.array(gt-pred))), cmin)
    
    else:
        vmax_abs = max(np.nanmax(np.array(gt)), cmin)
        if ar:
            vmax_diff = max(np.nanmax(np.abs(gt[0]-pred[ts])), cmin)          
        else:
            vmax_diff = max(np.nanmax(np.array(diff)), cmin)
            vmin_diff = min(np.nanmin(np.array(diff)), cmin)
    
    # if the range of water depth values is small
    if (vmax_abs-cmin)<0.1:
        colors_norm = colors.Normalize(vmin =cmin, vmax = vmax_abs)
    else:
        colors_norm = colors.LogNorm(vmin =cmin, vmax = vmax_abs)

    # show the complete catchment
    pos0 = axs[0].imshow(pred, cmap="Blues", norm=colors_norm)
    pos1 = axs[1].imshow(gt, cmap="Blues", norm=colors_norm)
    pos2 = axs[2].imshow(diff, cmap="RdBu", norm=colors.SymLogNorm(linthresh=0.01, vmin =-1, vmax=1))  

    if show_diff and ts>0:
        if ar:
            pos3 = axs[3].imshow(pred[ts] - gt[0], cmap="seismic", vmin=-vmax_diff, vmax = vmax_diff)  
            pos4 = axs[4].imshow(gt[ts] - gt[0], cmap="seismic", vmin=-vmax_diff, vmax = vmax_diff)  
            
        else:
            pos3 = axs[3].imshow(pred[ts] - gt[ts-1], cmap="seismic", vmin=-vmax_diff, vmax = vmax_diff)  
            pos4 = axs[4].imshow(gt[ts] - gt[ts-1], cmap="seismic", vmin=-vmax_diff, vmax = vmax_diff)  
    
    """ Set figure suptitle
    if ts is not None:   
        fig.suptitle("Visualization for model {} and timestep {} ".format(model_name.split('_')[-1], ts), fontsize = 14 ,fontweight="bold")
    if start_ts is not None and lead_time is not None: 
        title = model_name + ', start_timestep = '+ str(start_ts) + ', lead_time = ' + str(lead_time)
        fig.suptitle(title, fontsize = 14 ,fontweight="bold")
    """
    
    axs[0].set_title('Pred water depth [m]',fontweight="bold", fontsize=24)
    axs[0].tick_params(axis='both', which='major', labelsize=16)
    axs[1].set_title('GT water depth [m]',fontweight="bold", fontsize=24)
    axs[1].set_xticks([]), axs[1].set_yticks([])
    axs[1].tick_params(axis='both', which='major', labelsize=16)
    axs[2].set_title('Error: Pred-GT [m]',fontweight="bold", fontsize=24)
    axs[2].set_xticks([]), axs[2].set_yticks([])
    axs[2].tick_params(axis='both', which='major', labelsize=16)

    # define colorbar scalar formatter
    sFormatter = ticker.ScalarFormatter(useMathText=True)
    sFormatter.set_powerlimits((-1,2))
    #sFormatter = ticker.LogFormatterSciNotation(labelOnlyBase=True)

    # specify all colorbars
    #kwargs = {'format': '%.2f'}
    divider = make_axes_locatable(axs[0])
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar0 = fig.colorbar(pos0, cax=cax)
    cbar0.ax.tick_params(labelsize=16)
    cbar0.ax.yaxis.set_major_formatter(sFormatter)
    cbar0.ax.xaxis.set_major_formatter(sFormatter)
    #cbar0 = fig.colorbar(pos0, ax=axs[0], fraction=0.136, pad=0.02)#, extend='both', format="%0.2f")
    #cbar0.ax.tick_params(labelsize=12)
    #cbar0.ax.yaxis.set_major_formatter(sFormatter)
    #cbar0.ax.xaxis.set_major_formatter(sFormatter)
    #cbar0.formatter(sFormatter)
    divider = make_axes_locatable(axs[1])
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar1 = fig.colorbar(pos1, cax=cax)
    cbar1.ax.tick_params(labelsize=16)
    cbar1.ax.yaxis.set_major_formatter(sFormatter)
    cbar1.ax.yaxis.set_major_formatter(sFormatter)
    divider = make_axes_locatable(axs[2])
    cax = divider.append_axes("right", size="5%", pad=0.1)
    cbar2 = fig.colorbar(pos2, cax=cax)    
    cbar2.ax.tick_params(labelsize=16)
    cbar2.ax.yaxis.set_major_formatter(sFormatter)
    cbar2.ax.xaxis.set_major_formatter(sFormatter)


    if zoom is not None:
        axins0 = axs[0].inset_axes([0.55, 0.05, 0.45, 0.45])
        axins0.imshow(pred, cmap="Blues", norm=colors_norm)
        axins0.set_xlim(zoom[0], zoom[1])
        axins0.set_ylim(zoom[3], zoom[2])
        axins0.set_xticklabels([])
        axins0.set_yticklabels([])
        axins1 = axs[1].inset_axes([0.55, 0.05, 0.45, 0.45])
        axins1.imshow(gt, cmap="Blues", norm=colors_norm)
        axins1.set_xlim(zoom[0], zoom[1])
        axins1.set_ylim(zoom[3], zoom[2])
        axins1.set_xticklabels([])
        axins1.set_yticklabels([])
        axins2 = axs[2].inset_axes([0.55, 0.05, 0.45, 0.45])
        axins2.imshow(diff, cmap="RdBu", norm=colors.SymLogNorm(linthresh=0.01, vmin =-1, vmax=1))
        axins2.set_xlim(zoom[0], zoom[1])
        axins2.set_ylim(zoom[3], zoom[2])
        axins2.set_xticklabels([])
        axins2.set_yticklabels([])
        #axs[0].indicate_inset_zoom(axins0, edgecolor="black")
        #axs[1].indicate_inset_zoom(axins1, edgecolor="black")
        #axs[2].indicate_inset_zoom(axins2, edgecolor="black")
        my_mark_inset(axs[0], axins0, loc1a=1, loc1b=4, loc2a=3, loc2b=2, fc="none", ec="0.5")
        my_mark_inset(axs[1], axins1, loc1a=1, loc1b=4, loc2a=3, loc2b=2, fc="none", ec="0.5")
        my_mark_inset(axs[2], axins2, loc1a=1, loc1b=4, loc2a=3, loc2b=2, fc="none", ec="0.5")

                 
    if show_diff and ts>0:
        axs[3].set_title('Diff pred',fontweight="bold")
        axs[4].set_title('Diff GT',fontweight="bold")
        cbar3 = fig.colorbar(pos3, ax=axs[3], extend='both', fraction=0.136, pad=0.02)
        cbar4 = fig.colorbar(pos4, ax=axs[4], extend='both', fraction=0.136, pad=0.02)    
        
    if save_folder:
        name = re.sub('\W+', '', model_name)
        if start_ts:
            name = name + '_start_ts_'+ str(start_ts) 
        if ts is not None:
            name = name + '_input_ts_'+ str(ts) 
        if lead_time:
            name = name + '_lead_time_' + str(lead_time)
        if zoom:
            name = name + '_zoom'
        filename = save_folder  +  name + '.png'
        plt.tight_layout()
        plt.savefig(filename, dpi = 600, bbox_inches='tight')
    # plt.show()

def plot_2d_hist(pred_event, gt_event, mask, ts=0, save_folder=None, model_name=None):
    import matplotlib.colors as colors    
    if ts is not None:
        pred = pred_event[ts]
        gt = gt_event[ts]
    diff = pred - gt
    plt.figure(figsize=(8,8))
    plt.hist2d(gt.flatten(), diff.flatten(), bins=(50, 50), range=[[0,10],[-2,2]], cmap="Blues", norm=colors.LogNorm(vmin=1, vmax=10000))
    plt.xlim(0, 10)
    plt.ylim(-2,2)
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    plt.xlabel('GT water depth [m]', fontsize=24)
    plt.ylabel('Error: Pred-GT [m]', fontsize=25)
    plt.tight_layout()
    name = re.sub('\W+', '', model_name)
    name = name + '_input_ts_'+ str(ts)
    filename = save_folder  +  name + '.jpg'
    plt.savefig(filename, dpi = 1200)
    
############################################################################################################################################################
##################################################################  Movies ############################################################################ 

# def numpy2movie(predictions, ground_t, fps=10):
    
#     '''
#     Plots all predicted timesteps in validation data
#     '''
#     import matplotlib.pyplot as plt
#     from moviepy.editor import VideoClip
#     from moviepy.video.io.bindings import mplfig_to_npimage
#     duration = len(predictions)/fps
#     fig = plt.figure(figsize=(12,4))
#     vmax= np.max(ground_t)
#     vmin = np.min(ground_t)
#     vmax_d = np.max(np.abs(predictions-ground_t))
#     def make_frame(t):

#         i = int(np.floor(t*fps))
#         ax1 = plt.subplot(131)
#         ax1.clear()
#         plt.imshow(ground_t[i], vmin=vmin, vmax=vmax)
#         plt.colorbar()
#         plt.title("Ground truth")
#         plt.xticks([])
#         plt.yticks([])
        
#         ax2 = plt.subplot(132)
#         ax2.clear()
#         plt.imshow(predictions[i], vmin=vmin, vmax=vmax)
#         plt.colorbar()     

#         plt.title("Prediction")
#         plt.xticks([])
#         plt.yticks([])
        
#         ax3 = plt.subplot(133)
#         ax3.clear()
#         plt.imshow(np.abs(predictions[i]-ground_t[i]), vmin=0, vmax=vmax_d)
#         plt.colorbar()
#         plt.title("Difference")
#         plt.xticks([])
#         plt.yticks([])
        

#         return mplfig_to_npimage(fig)

#     animation = VideoClip(make_frame, duration=duration)
    
#     return animation

def numpy2movie(predictions, ground_t, fps=10):
    
    '''
    Plots all predicted timesteps in validation data
    '''
    import matplotlib.pyplot as plt
    from moviepy.editor import VideoClip
    from moviepy.video.io.bindings import mplfig_to_npimage
    import matplotlib.colors as colors
    duration = len(predictions)/fps
    fig = plt.figure(figsize=(12,4))
    vmax= np.max(ground_t)
    # vmin = np.min(ground_t)
    vmax_d = np.max(np.abs(predictions-ground_t))
    cmap = "hot_r"
    cmin = 0.01
    def make_frame(t):
    
        i = int(np.floor(t*fps))
        ax1 = plt.subplot(131)
        ax1.clear()
        plt.imshow(ground_t[i], cmap=cmap, norm=colors.LogNorm(vmin =cmin, vmax = vmax))
        plt.colorbar()
        plt.title("Ground truth")
        plt.xticks([])
        plt.yticks([])
        
        ax2 = plt.subplot(132)
        ax2.clear()
        plt.imshow(predictions[i], cmap=cmap, norm=colors.LogNorm(vmin =cmin, vmax = vmax))
        plt.colorbar()     

        plt.title("Prediction")
        plt.xticks([])
        plt.yticks([])
        
        ax3 = plt.subplot(133)
        ax3.clear()
        plt.imshow(np.abs(predictions[i]-ground_t[i]), cmap=cmap, norm=colors.LogNorm(vmin = cmin, vmax = vmax_d))
        plt.colorbar()
        plt.title("Difference")
        plt.xticks([])
        plt.yticks([])
        

        return mplfig_to_npimage(fig)

    animation = VideoClip(make_frame, duration=duration)
    
    return animation



def mat2movie(mat, fps=10):
    
    '''
    Plots all predicted timesteps in validation data
    '''
    import matplotlib.pyplot as plt
    from moviepy.editor import VideoClip
    from moviepy.video.io.bindings import mplfig_to_npimage
    duration = len(mat)/fps
    vmax= np.max(mat)
    vmin = np.min(mat)
    fig = plt.figure()
    ax1 = plt.gca()
    def make_frame(t):
        ax1.clear()
        i = int(np.floor(t*fps))
        ax1.imshow(mat[i], vmin=vmin, vmax=vmax)
        ax1.set_xticks([])
        ax1.set_yticks([])
        
        return mplfig_to_npimage(fig)

    animation = VideoClip(make_frame, duration=duration)
    
    return animation

