import torch

def l1_loss_weight(predictions, target):
    mask = predictions['mask']
    predictions =  predictions['y_pred']
    target = target
    preds = torch.mul(predictions, mask)
    loss = torch.abs(preds  - target)
    idx = target > 0.2
    loss[idx] = loss[idx] *4
    loss = loss.sum()/(torch.sum(mask.float()))
    return loss   # compute the mean only considering elemnts in the mask

def l2_loss(predictions, target):
    mask = predictions['mask']
    predictions =  predictions['y_pred']
    preds = torch.mul(predictions, mask)
    loss = torch.mean(torch.square(preds  - target))
    return loss   # compute the mean only considering elemnts in the mask

def l1_loss(predictions, target):
    mask = predictions['mask']
    predictions =  predictions['y_pred']
    preds = torch.mul(predictions, mask)
    loss = torch.abs(preds  - target)
    loss = loss.sum()/(torch.sum(mask.float()))
    return loss   # compute the mean only considering elemnts in the mask

def l1_loss_multiple_outs(predictions, target):
    mask = predictions['mask']
    predictions =  predictions['y_pred']
    preds = torch.mul(predictions, mask)
    loss = torch.abs(preds  - target)
    loss = loss.sum()/(torch.sum(mask.float()))/3
    return loss

