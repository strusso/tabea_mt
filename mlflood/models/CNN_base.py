import torch.nn as nn
import torch.nn.functional as F
import torch
    

class CNN_base(nn.Module):
    """
    CNN with 3 convolutional layers
    
    """

    def __init__(self, k = 64, border_size=0, timestep = 2, n_input_channels=3, use_diff_dem=True, use_topo_index=True, ts_out = None):
        super(CNN_base, self).__init__()
        
        self.use_diff_dem = use_diff_dem
        self.use_topo_index=use_topo_index
        self.timestep = timestep
        self.input_channels =  n_input_channels

        self.ts_out = ts_out   # number of output channels
        self.conv1 = nn.Conv2d(self.input_channels, k, 5, padding=(2,2))
        self.conv2 = nn.Conv2d(k, k, 5, padding=(2,2))
        if ts_out:
            self.conv3 = nn.Conv2d(k, ts_out, 5, padding=(2, 2))   # if ts_out > 1, it will return a higher number of output channels
        else:
            self.conv3 = nn.Conv2d(k, 1, 5, padding=(2, 2))
        self.border_size = border_size

    def forward(self, x, mask, *args):
        x1 = F.relu(self.conv1(x))
        x2 = F.relu(self.conv2(x1))
        x3 = self.conv3(x2)
        if self.border_size:
            x3 = x3[:,:,self.border_size:-self.border_size, self.border_size:-self.border_size]
            mask = mask[:,:,self.border_size:-self.border_size, self.border_size:-self.border_size] 
        return {"y_pred": x3, "mask":mask}
   

