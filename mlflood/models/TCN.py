import os

import torch
import torch.nn as nn
import torch.utils.data

"""
Pytorch re-implementation of Pelletier et al. 2019
https://github.com/charlotte-pel/temporalCNN
https://www.mdpi.com/2072-4292/11/5/523
"""

class TempCNN(torch.nn.Module):
    # input shape: 
    def __init__(self, input_ts=5, out_ts=1, in_channels=10, out_channels=1, kernel_size=3, hidden_dims=128, dropout=0.18):
        super(TempCNN, self).__init__()
        self.hidden_dims = hidden_dims

        self.conv_bn_relu1 = Conv3D_BatchNorm_Relu_Dropout(input_ts, hidden_dims, kernel_size=kernel_size,
                                                           drop_probability=dropout)
        self.conv_bn_relu2 = Conv3D_BatchNorm_Relu_Dropout(hidden_dims, out_ts, kernel_size=kernel_size,
                                                           drop_probability=dropout)

        self.flatten = Flatten()
        self.conv2d_1 = Conv2D_BatchNorm_Relu_Dropout(in_channels, hidden_dims, kernel_size=kernel_size)
        self.conv2d_2 = Conv2D_BatchNorm_Relu_Dropout(hidden_dims, hidden_dims, kernel_size=kernel_size)
        self.outconv = Conv2D_BatchNorm_Relu_Dropout(hidden_dims, out_channels=out_channels, kernel_size=kernel_size)

    def forward(self, x, mask=None):
        # require BxTxCxHxW
        x = x.transpose(1,2)
        x = self.conv_bn_relu1(x)
        x = self.conv_bn_relu2(x)
        # remove time dimension
        x = self.flatten(x)
        x = self.conv2d_1(x)
        x = self.conv2d_2(x)
        x = self.outconv(x)
        return {"y_pred": x, "mask":mask}

    def save(self, path="model.pth", **kwargs):
        print("\nsaving model to " + path)
        model_state = self.state_dict()
        os.makedirs(os.path.dirname(path), exist_ok=True)
        torch.save(dict(model_state=model_state, **kwargs), path)

    def load(self, path):
        print("loading model from " + path)
        snapshot = torch.load(path, map_location="cpu")
        model_state = snapshot.pop('model_state', snapshot)
        self.load_state_dict(model_state)
        return snapshot


class Conv3D_BatchNorm_Relu_Dropout(torch.nn.Module):
    def __init__(self, input_dim, hidden_dims, kernel_size=5, drop_probability=0.5):
        super(Conv3D_BatchNorm_Relu_Dropout, self).__init__()

        self.block = nn.Sequential(
            nn.Conv3d(input_dim, hidden_dims, kernel_size, padding=(kernel_size // 2)),
            nn.InstanceNorm3d(hidden_dims),
            nn.ReLU(),
            nn.Dropout(p=drop_probability)
        )

    def forward(self, X):
        return self.block(X)


class Conv2D_BatchNorm_Relu_Dropout(torch.nn.Module):
    def __init__(self, in_channels, out_channels, kernel_size, drop_probability=0.5):
        super(Conv2D_BatchNorm_Relu_Dropout, self).__init__()

        self.block = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size, padding=(kernel_size // 2)),
            nn.InstanceNorm2d(out_channels),
            nn.ReLU(),
            nn.Dropout(p=drop_probability)
        )

    def forward(self, X):
        return self.block(X)


class Flatten(nn.Module):
    def forward(self, input):
        return input.squeeze()