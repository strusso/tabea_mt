from torch.nn import Module
import torch

class AutoRegressiveBaseline(Module):
    def __init__(self, border_size=0, timestep = 5, use_diff_dem=True, kernel_size=1):
        super(AutoRegressiveBaseline, self).__init__()
        self.border_size = border_size
        self.timestep = timestep
        self.use_diff_dem = use_diff_dem

        input_channels = timestep*2 - 1 # wd*timestep, wd_diff*(timestep-1)
        padding = (kernel_size - 1) // 2
        self.conv = torch.nn.Conv2d(in_channels=input_channels, out_channels=1, kernel_size=kernel_size, padding=padding)

    def forward(self, x, mask, *args):
        b = self.border_size
        ts = self.timestep

        wd = x[:, -2*ts+1:-ts+1]
        wd_diff = x[:,-ts+1:]
        inputs = torch.cat([wd, wd_diff], axis=1)

        out = self.conv(inputs)

        out = torch.cat([out, mask], 1)
        if b:
            out = out[:,:,b:-b,b:-b]
        return out