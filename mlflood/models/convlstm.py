# code adapted from: https://sladewinter.medium.com/video-frame-prediction-using-convlstm-network-in-pytorch-b5210a6ce582

from re import I
from turtle import xcor
import torch
import torch.nn as nn
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


# Original ConvLSTM cell as proposed by Shi et al.
class ConvLSTMCell(nn.Module):

    def __init__(self, in_channels, out_channels, 
    kernel_size, padding, activation, patch_size):

        super(ConvLSTMCell, self).__init__()  

        if activation == "tanh":
            self.activation = torch.tanh 
        elif activation == "relu":
            self.activation = torch.relu
        
        # Idea adapted from https://github.com/ndrplz/ConvLSTM_pytorch
        self.conv = nn.Conv2d(
            in_channels=in_channels + out_channels, 
            out_channels=4 * out_channels, 
            kernel_size=kernel_size, 
            padding=padding)           

        # Initialize weights for Hadamard Products
        self.W_ci = nn.Parameter(torch.Tensor(out_channels, *patch_size))
        self.W_co = nn.Parameter(torch.Tensor(out_channels, *patch_size))
        self.W_cf = nn.Parameter(torch.Tensor(out_channels, *patch_size))

    def forward(self, X, H_prev, C_prev):

        # Idea adapted from https://github.com/ndrplz/ConvLSTM_pytorch
        conv_output = self.conv(torch.cat([X, H_prev], dim=1))

        # Idea adapted from https://github.com/ndrplz/ConvLSTM_pytorch
        i_conv, f_conv, C_conv, o_conv = torch.chunk(conv_output, chunks=4, dim=1)
        input_gate = torch.sigmoid(i_conv + self.W_ci * C_prev )
        forget_gate = torch.sigmoid(f_conv + self.W_cf * C_prev )

        # Current Cell output
        C = forget_gate*C_prev + input_gate * self.activation(C_conv)

        output_gate = torch.sigmoid(o_conv + self.W_co * C )

        # Current Hidden State
        H = output_gate * self.activation(C)

        return H, C

class ConvLSTM(nn.Module):

    """
    Parameters:
        in_channels: Number of channels in input
        out_channels: Number of hidden channels
        kernel_size: Size of kernel in convolutions
        padding: according to the kernel size
        activation function:
        patch_size: patch size

    Input:
        Frame sequence: B, C, T, H, W
        batch_size, num_channels, seq_len, height, width

    Output:
      
    """

    def __init__(self, in_channels, out_channels, 
    kernel_size, padding, activation, patch_size, mnist):

        super(ConvLSTM, self).__init__()

        self.out_channels = out_channels
        self.mnist = mnist

        # We will unroll this over time steps
        self.convLSTMcell = ConvLSTMCell(in_channels, out_channels, 
        kernel_size, padding, activation, patch_size)

    def forward(self, X, states=None):

        # Get the input dimensions
        batch_size, _, seq_len, height, width = X.size()

        # Initialize output
        output = torch.zeros(batch_size, self.out_channels, seq_len, height, width, device=device)
        
        # Initialize hidden and cell states
        if states is None:
            H  = torch.zeros(batch_size, self.out_channels, height, width, device=device)
            C = torch.zeros(batch_size,self.out_channels, height, width, device=device)
        
        else:
            H, C = states

        """
        
        if not self.mnist:
            output = output.to(torch.float16)
            H = H.to(torch.float16)
            C = C.to(torch.float16)
            """
          
        # Unroll over time steps
        for time_step in range(seq_len):

            H, C = self.convLSTMcell(X[:,:,time_step], H, C)

            output[:,:,time_step] = H

        return output

       
class convlstm_seq(nn.Module):
    """
    class for the complete network, with multiple ConvLSTM, batchnorm and convolution layers

    Example: model = Seq2Seq(num_channels=4, num_kernels=64, kernel_size=(3, 3), padding=(1, 1), 
    activation="relu", patch_size=(256, 256), num_layers=2).to(device)
    
    """

    def __init__(self, num_channels=4, num_kernels=32, kernel_size=(5, 5), 
    num_layers=3, activation="relu", patch_size=(256, 256), mnist=False):

        super(convlstm_seq, self).__init__()
        self.padding = kernel_size[0] // 2, kernel_size[1] // 2
        self.mnist = mnist

        # First layer (Different in_channels than the rest)
        self.convlstm1 = ConvLSTM(
                in_channels=num_channels, out_channels=num_kernels,
                kernel_size=kernel_size, padding=self.padding, 
                activation=activation, patch_size=patch_size, mnist=mnist)
        self.batchnorm1 = nn.BatchNorm3d(num_features=num_kernels)

        #  Second layer 
        self.convlstm2 = ConvLSTM(
                    in_channels=num_kernels, out_channels=num_kernels,
                    kernel_size=kernel_size, padding=self.padding, 
                    activation=activation, patch_size=patch_size, mnist=mnist)       
        self.batchnorm2 = nn.BatchNorm3d(num_features=num_kernels)

        #  Third ConvLSTM layer 
        self.convlstm3 = ConvLSTM(
                    in_channels=num_kernels, out_channels=num_kernels,
                    kernel_size=kernel_size, padding=self.padding, 
                    activation=activation, patch_size=patch_size, mnist=mnist)       
        self.batchnorm3 = nn.BatchNorm3d(num_features=num_kernels)

        # layers
        if num_layers == 1:
            self.layers = nn.Sequential(self.convlstm1, self.batchnorm1)
        elif num_layers == 2:
            self.layers = nn.Sequential(self.convlstm1, self.batchnorm1, self.convlstm2, self.batchnorm2)
        else:
            self.layers = nn.Sequential(self.convlstm1, self.batchnorm1, self.convlstm2, self.batchnorm2, self.convlstm3, self.batchnorm3)

        # Convolutional Layer to predict output frame
        self.conv = nn.Conv2d(
            in_channels=num_kernels, out_channels=1,
            kernel_size=kernel_size, padding=self.padding)


    def forward(self, X, mask=None):
        # Forward propagation through all the layers
        if hasattr(self, "layers"):
            x = self.layers(X)
        else:
            layers = nn.Sequential(self.convlstm1, self.batchnorm1, self.convlstm2, self.batchnorm2, self.convlstm3, self.batchnorm3)
            x = layers(X)
        
        # Convolution of the hidden states for the last timestep
        output = self.conv(x[:,:,-1])
        
        # for mnist: final sigmoid layer
        if self.mnist:
            return nn.Sigmoid()(output)

        # return the hidden convlstm states to use model in forecasting mode
        else:
            return {"y_pred": output, "mask":mask}