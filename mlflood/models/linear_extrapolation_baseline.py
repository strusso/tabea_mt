from torch.nn import Module
import torch

from mlflood.dataset import unnormalize

class LinearExtrapolation(Module):
    def __init__(self, border_size=0, timestep=5, normalize_output=False):
        super(LinearExtrapolation, self).__init__()
        self.border_size = border_size
        self.timestep = timestep
        self.normalize_output = normalize_output

    def forward(self, x, mask, *args):
        b = self.border_size
        out = x[:, -1:]
        if not self.normalize_output:
            out = unnormalize(out) + x[:, -self.timestep:-self.timestep+1]

        out = torch.cat([out, mask], 1)
        if b:
            out = out[:,:,b:-b,b:-b]
        return out