from .utils import get_comp_mat
import math
import numpy as np
import torch
from torch import nn
import torch.nn.functional as F
from timm.models.layers import to_ntuple

# ############################
# '''Hypercomplex Linear!'''
# ###########################
class HyperLinear(nn.Module):
    def __init__(self, in_features, out_features, bias=True, n_divs=4, ignore_print=None, name=None):
        super().__init__()
        self.in_features = in_features
        self.out_features = out_features
        self.n_divs = n_divs
        self.weight = nn.Parameter(torch.rand(size=(n_divs, self.out_features // n_divs, self.in_features // n_divs)))
        if bias:
            self.bias = nn.Parameter(torch.zeros(self.out_features))
        else:
            self.register_parameter('bias', None)

        self.my_list = ['in_features', 'out_features', 'bias', 'n_divs']
        self.ignore_print = [] if ignore_print is None else ignore_print
        self.name = name

        self.comp_mat = get_comp_mat(n_divs)
        self.reset_parameters()

    def reset_parameters(self):
        stdv = math.sqrt(6.0 / (self.weight.size(1) + self.weight.size(2) * self.n_divs))
        self.w_shape = self.weight.shape

        phase = np.random.uniform(low=-np.pi, high=np.pi, size=self.w_shape[1:]).astype(np.float32)
        phi = np.random.uniform(low=-1, high=1, size=(self.n_divs - 1, *self.w_shape[1:])).astype(np.float32)
        phi /= np.sqrt((phi ** 2 + 1e-4).sum(axis=0))
        modulus = chi.rvs(df=self.n_divs, loc=0, scale=stdv, size=self.w_shape[1:]).astype(np.float32)

        weight = [modulus * np.cos(phase)]
        weight.extend([modulus * phi_ * np.sin(phase) for phi_ in phi])

        self.weight.data = torch.from_numpy(np.stack(weight))

        # self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.zero_()

    def forward(self, x):
        hamilton = fast_hypercomplex(self.weight, self.n_divs, comp_mat=self.comp_mat)
        return F.linear(x, hamilton, self.bias)

    def extra_repr(self) -> str:
        extra = ', '.join(
            [f"{key}={self.__getattribute__(key) if key is not 'bias' else str(self.bias is not None)}"
             for key in self.my_list if key not in self.ignore_print])
        return extra

    # def extra_repr(self) -> str:
    #     return f"in_features={self.in_features}, out_features={self.out_features}, " + \
    #            f"bias={self.bias is not None}, n_divs={self.n_divs}"
    def __repr__(self):
        name = self.name or 'HyperLinear'
        return f"{name}({self.extra_repr()})"


class HyperLinear_(nn.Linear):
    def __init__(self, in_features, out_features, bias=True, n_divs=4):
        super().__init__(in_features, out_features, bias=bias)
        self.in_features = in_features
        self.out_features = out_features
        self.n_divs = n_divs
        # self.weight = nn.Parameter(torch.FloatTensor(self.in_features // n_divs, self.out_features))
        self.weight = nn.Parameter(torch.rand(size=(n_divs, self.out_features // n_divs, self.in_features // n_divs)))
        if bias:
            self.bias = nn.Parameter(torch.zeros(self.out_features))
        else:
            self.register_parameter('bias', None)

        self.comp_mat = get_comp_mat(n_divs)
        self._reset_parameters()
        register_parametrization(self, 'weight', Hamilton(n_divs=self.n_divs, comp_mat=self.comp_mat))

    def _reset_parameters(self):
        stdv = math.sqrt(6.0 / (self.weight.size(1) + self.weight.size(2) * self.n_divs))
        self.weight.data.uniform_(-stdv, stdv)
        if self.bias is not None:
            self.bias.data.zero_()

    def extra_repr(self) -> str:
        return f"in_features={self.in_features}, out_features={self.out_features}, " + \
               f"bias={self.bias is not None}, n_divs={self.n_divs}"



# ############################
# '''Hypercomplex Conv'''
# ############################
class HyperConv(nn.Module):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, n_divs=4,
                 dilation=1, padding=0, groups=1, bias=True, init_criterion='he',
                 weight_init='hypercomplex', operation='convolution2d',):
                 # hypercomplex_format=True):

        super(HyperConv, self).__init__()
        n = int(''.join(c for c in operation if c.isdigit()))
        self.n_divs = n_divs
        self.in_channels = in_channels   # // self.n_divs
        self.out_channels = out_channels  # // self.n_divs
        self.stride = to_ntuple(n)(stride)
        self.padding = to_ntuple(n)(padding)
        self.groups = groups
        self.dilation = to_ntuple(n)(dilation)
        self.init_criterion = init_criterion
        self.weight_init = weight_init
        self.operation = operation

        # self.hypercomplex_format = hypercomplex_format
        # self.winit = {'hypercomplex': hypercomplex_init,
        #               'unitary': unitary_init,
        #               'random': random_init}[self.weight_init]

        assert (self.in_channels % self.groups == 0)
        assert (self.out_channels % self.groups == 0)

        self.kernel_size, self.w_shape = self.get_kernel_and_weight_shape(kernel_size)

        self.weight = nn.Parameter(torch.Tensor(*self.w_shape))

        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))
        else:
            self.register_parameter('bias', None)

        self.comp_mat = get_comp_mat(n_divs)
        self.reset_parameters()
        # changes
        # self.old_device = self.weight.device
        # self.cat_kernels_hypercomplex = None  # make_hypercomplex_mul(self.weight, n_divs=self.n_divs, comp_mat=self.comp_mat)
        # self.cat_kernels_hypercomplex = make_hypercomplex_mul(self.weight, n_divs=self.n_divs, comp_mat=self.comp_mat)

    def get_kernel_and_weight_shape(self, kernel_size):
        if self.operation == 'convolution1d':
            if type(kernel_size) is not int:
                raise ValueError(
                    """An invalid kernel_size was supplied for a 1d convolution. The kernel size
                    must be integer in the case. Found kernel_size = """ + str(kernel_size)
                )
            else:
                # ks = kernel_size
                ks = tuple((kernel_size,))
                # w_shape = (self.out_channels // self.n_divs, self.in_channels // self.groups) + (*ks,)
        else:  # in case it is 2d or 3d.
            if self.operation == 'convolution2d' and type(kernel_size) is int:
                ks = to_ntuple(2)(kernel_size)  # (kernel_size, kernel_size)
            elif self.operation == 'convolution3d' and type(kernel_size) is int:
                ks = to_ntuple(3)(kernel_size)  # (kernel_size, kernel_size, kernel_size)
            elif type(kernel_size) is not int:
                if self.operation == 'convolution2d' and len(kernel_size) != 2:
                    raise ValueError(
                        """An invalid kernel_size was supplied for a 2d convolution. The kernel size
                        must be either an integer or a tuple of 2. Found kernel_size = """ + str(kernel_size)
                    )
                elif self.operation == 'convolution3d' and len(kernel_size) != 3:
                    raise ValueError(
                        """An invalid kernel_size was supplied for a 3d convolution. The kernel size
                        must be either an integer or a tuple of 3. Found kernel_size = """ + str(kernel_size)
                    )
                else:
                    ks = kernel_size
            # w_shape = (out_channels, in_channels // self.groups) + (*ks,)
        w_shape = (self.n_divs, self.out_channels // self.n_divs, self.in_channels // self.groups // self.n_divs) +\
                  (*ks,)
        return ks, w_shape

    def reset_parameters(self):
        # print(self.w_shape)
        receptive_field = np.prod(self.w_shape[2:])
        fan_in = self.n_divs * self.w_shape[1] * receptive_field
        fan_out = self.w_shape[0] * receptive_field

        if self.init_criterion == 'glorot':
            stdv = np.sqrt(2 / (fan_in + fan_out))
        elif self.init_criterion == 'he':
            stdv = np.sqrt(2 / fan_in)
        else:
            raise ValueError('Invalid criterion: ' + self.init_criterion)
        # print(stdv)
        if self.weight_init == 'hypercomplex':
            stdv /= np.sqrt(self.n_divs)
        # print(stdv)
        self.weight.data.uniform_(-stdv, stdv)

        if self.bias is not None:
            self.bias.data.zero_()


    # def cuda(self, device=None):
    #     super().cuda(device)
    #     if self.weight.device != self.cat_kernels_hypercomplex.device:
    #         # self.old_device = self.weight.device
    #         self.cat_kernels_hypercomplex = self.cat_kernels_hypercomplex.to(self.weight.device)
    #
    # def to(self, device=..., dtype=..., non_blocking=...):
    #     super().to(device, dtype, non_blocking)
    #     if self.weight.device != self.cat_kernels_hypercomplex.device:
    #         # self.old_device = self.weight.device
    #         self.cat_kernels_hypercomplex = self.cat_kernels_hypercomplex.to(self.weight.device)

    def forward(self, x):
        assert 3 <= x.dim() <= 5, "The convolutional input x is either 3, 4 or 5 dimensional. x.dim = " + str(
            x.dim())
        # print(self.old_device, self.weight.device)
        # self.cat_kernels_hypercomplex = self.cat_kernels_hypercomplex.to(self.weight.device)
        self.cat_kernels_hypercomplex = fast_hypercomplex(self.weight, n_divs=self.n_divs, comp_mat=self.comp_mat)
        # if self.weight.device != self.cat_kernels_hypercomplex.device:
        #     # self.old_device = self.weight.device
        #     # self.cat_kernels_hypercomplex = self.cat_kernels_hypercomplex.to(self.weight.device)
        #     self.cat_kernels_hypercomplex = make_hypercomplex_mul(self.weight, n_divs=self.n_divs, comp_mat=self.comp_mat)
        # else:
        #     self.cat_kernels_hypercomplex = self.cat_kernels_hypercomplex
        # self.cat_kernels_hypercomplex = make_hypercomplex_mul(self.weight, n_divs=self.n_divs, comp_mat=self.comp_mat)  # updated 28May2021 for speed
        convfunc = {3: F.conv1d, 4: F.conv2d, 5: F.conv3d}[x.dim()]
        return convfunc(x, self.cat_kernels_hypercomplex, self.bias, self.stride, self.padding, self.dilation,
                        self.groups)  # + convfunc(x, self.cat_kernels_hypercomplex[1], None, self.stride, self.padding,
                                                # self.dilation, self.groups)

    def extra_repr(self) -> str:
        return 'in_channels=' + str(self.in_channels) \
               + ', out_channels=' + str(self.out_channels) \
               + ', bias=' + str(self.bias is not None) \
               + ', kernel_size=' + str(self.kernel_size) \
               + ', stride=' + str(self.stride) \
               + ', padding=' + str(self.padding) \
               + ', init_criterion=' + str(self.init_criterion) \
               + ', weight_init=' + str(self.weight_init) \
               + ', n_divs=' + str(self.n_divs) \
               + ', operation=' + str(self.operation)


class HyperConv1d(HyperConv):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, n_divs=4,
                 dilation=1, padding=0, groups=1, bias=True, init_criterion='he',
                 weight_init='hypercomplex'):
        super().__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                         stride=stride, n_divs=n_divs,
                         dilation=dilation, padding=padding, groups=groups, bias=bias, init_criterion=init_criterion,
                         weight_init=weight_init, operation='convolution1d')

    def __repr__(self):
            config = super().__repr__()
            config = config.replace(f', operation={str(self.operation)}', '')
            return config


class HyperConv2d(HyperConv):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, n_divs=4,
                 dilation=1, padding=0, groups=1, bias=True, init_criterion='he',
                 weight_init='hypercomplex'):
        super().__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                         stride=stride, n_divs=n_divs,
                         dilation=dilation, padding=padding, groups=groups, bias=bias, init_criterion=init_criterion,
                         weight_init=weight_init, operation='convolution2d')

    def __repr__(self):
            config = super().__repr__()
            config = config.replace(f', operation={str(self.operation)}', '')
            return config


class HyperConv3d(HyperConv):

    def __init__(self, in_channels, out_channels, kernel_size, stride=1, n_divs=4,
                 dilation=1, padding=0, groups=1, bias=True, init_criterion='he',
                 weight_init='hypercomplex'):
        super().__init__(in_channels=in_channels, out_channels=out_channels, kernel_size=kernel_size,
                         stride=stride, n_divs=n_divs,
                         dilation=dilation, padding=padding, groups=groups, bias=bias, init_criterion=init_criterion,
                         weight_init=weight_init, operation='convolution3d')

    def __repr__(self):
            config = super().__repr__()
            config = config.replace(f', operation={str(self.operation)}', '')
            return config


class Concatenate(nn.Module):
    def __init__(self, dim=-1, n_divs=1):
        super(Concatenate, self).__init__()
        self.n_divs = n_divs
        self.dim = dim

    def forward(self, x):
        # dim = x[0].size(self.dim) // self.n_divs
        # x_splits = [torch.split(x_i, [dim for _ in range(self.n_divs)], dim=self.dim) for x_i in x]
        x_splits = [torch.chunk(x_i, chunks=self.n_divs, dim=self.dim) for x_i in x]
        components = [torch.cat([x_split[component] for x_split in x_splits], dim=self.dim) for component
                      in range(self.n_divs)]
        # components = [torch.cat([get_c(x_i, component, self.n_divs) for x_i in x], dim=self.dim) for component
        #               in range(self.n_divs)]
        return torch.cat(components, dim=self.dim)


class HyperSoftmax(nn.Module):
    def __init__(self, dim=-1, n_divs=4):
        super().__init__()
        self.softmax = nn.Softmax(dim=dim)
        self.n_divs = n_divs
        self.dim = dim
        
    def forward(self, x):
        # x_dim = list(x.shape)
        dim = x.size(self.dim) // self.n_divs
        x_split = torch.split(x, [dim for _ in range(self.n_divs)], dim=self.dim)
        components = [self.softmax(x_split[component]) for
                      component in range(self.n_divs)]
        return torch.cat(components, dim=self.dim)