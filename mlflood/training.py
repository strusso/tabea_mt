import numpy as np
import matplotlib.pyplot as plt
from mlflood.simulation import create_random_dem, simulate, create_sims_dataset
import torch
import matplotlib.pyplot as plt
from poutyne import Model, SKLearnMetrics
from sklearn.metrics import r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test
from torchsummary import summary
from torch.utils.tensorboard import SummaryWriter
import datetime
import time
import copy
from torch import nn, optim


def training_loop(model, optimizer, dataloaders_train, dataloaders_valid, optimization_kwargs, device, path_loc, start_epoch=1, best_loss=np.Inf):
    
    '''
    Model training loop 
    
    '''
    if path_loc:
        writer = SummaryWriter(path_loc)
    
    time_start = time.time()
    loss_f = optimization_kwargs['loss_function']

    # initialize training historz
    history = {}
    history['train_loss'] = []
    history['val_loss'] = []

    best_model_wts = copy.deepcopy(model.state_dict())

    # throw a warning if gradients explode
    torch.autograd.set_detect_anomaly(True)

    train_idx = 0
    for epoch in range(start_epoch, optimization_kwargs["epochs"]+1):

        # training
        train_running_loss = 0.0
        train_acc = 0.0
        model.train()
        for train_idx,local_batch in enumerate(dataloaders_train):
            # Transfer to GPU
            local_x = local_batch['x'].to(device) 
            local_y = local_batch['y'].to(device) 
            local_mask = local_batch['mask'].to(device) 
            predictions = model(local_x, local_mask)
            loss = loss_f(predictions, local_y)
            optimizer.zero_grad()        # backprop
            loss.backward()
            optimizer.step()      # update model params
            train_running_loss += loss.detach().item()  # Compute avg loss and accuracy 

        train_loss = train_running_loss/(train_idx+1)
        history['train_loss'].append(train_loss)
        if path_loc:
            writer.add_scalar('train_loss', train_loss, epoch)

        # validation
        val_running_loss = 0.0
        val_acc = 0.0
        model.eval()
        for val_idx, local_batch in enumerate(dataloaders_valid):

            local_x = local_batch['x'].to(device) 
            local_y = local_batch['y'].to(device) 
            local_mask = local_batch['mask'].to(device) 
            predictions = model(local_x, local_mask) 

            val_loss = loss_f(predictions, local_y) 
            val_running_loss += val_loss.detach().item()

        val_loss =  val_running_loss/(val_idx+1)   

        history['val_loss'].append(val_loss)
        if path_loc:
            writer.add_scalar('val_loss', val_loss, epoch)

        if epoch == 1 or epoch % 5 == 0:
            print('Epoch: %d , train loss: %.5f , val loss: %.5f'% (epoch, train_loss,val_loss))

        if val_loss < best_loss:
            best_loss = val_loss
            best_model_wts = copy.deepcopy(model.state_dict())
            if path_loc:
                model_p = path_loc + "/checkpoint.pth"
                torch.save({
                    "model": model,
                    "model_state": model.state_dict(), 
                    "best_loss": best_loss,
                    "epoch": epoch,
                    "optimizer_state": optimizer.state_dict()},
                    model_p)

    time_elapsed = time.time() - time_start
    print('Training complete in {:.0f}m {:.0f}s'.format(
                time_elapsed // 60, time_elapsed % 60))  
    
    if path_loc:
        model.load_state_dict(best_model_wts)  # load best model parameters for saving
        writer.close()

    return model, history