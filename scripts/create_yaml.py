from datetime import date
import datetime
import yaml
import os

from mlflood.loss import l1_loss_weight, l1_loss, l2_loss

folder_name = 'experiments/20220912-exp_lead12-709_long/'

#folder
if not os.path.exists(folder_name):
    os.mkdir(folder_name)

#configurations
catchment_num = "709_long"
border_size = 0
timestep = 1
accumulate_rain = False
add_dry_timesteps = 4
model_name = "simvp"
use_diff_dem = False
use_topo_index = True
lead_time = 12
ts_out = 1
sample_type = "single"
dim_patch = 256
# convlstm required for ConvLSTM, SimVP, TCN, SwinUNet 
convlstm = 5
precision = 32
tau = 0.5

# validation on another catchment
catchment_valid = "709_long"

# model_kwargs
model_kwargs = {}
model_kwargs["model_name"] = model_name

# catchment_kwargs
catchment_kwargs = {}
catchment_kwargs["tau"] = tau
catchment_kwargs["timestep"] = timestep
catchment_kwargs["accumulate_rain"] = accumulate_rain
catchment_kwargs["add_dry_timesteps"] = add_dry_timesteps
catchment_kwargs["sample_type"] = sample_type
catchment_kwargs["dim_patch"] = dim_patch
catchment_kwargs["fix_indexes"] = False
catchment_kwargs["border_size"] = border_size
catchment_kwargs["use_diff_dem"] = use_diff_dem
catchment_kwargs["use_topo_index"] = use_topo_index
catchment_kwargs["ts_out"] = ts_out
catchment_kwargs["lead_time"] = lead_time
catchment_kwargs["convlstm"] = convlstm
catchment_kwargs['precision'] = precision

# optimization paramters
optimization_kwargs = {}
optimization_kwargs["batch_size"] = 4
optimization_kwargs["epochs"] = 20000
optimization_kwargs["learning_rate"] = 0.0001
optimization_kwargs["weight_d"] = 0
optimization_kwargs["loss_function"] = l2_loss

date = datetime.datetime.now().strftime("%Y%m%d")
#date = "20220803"
experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}-convlstm-{convlstm}"

if not os.path.exists(folder_name + experiment_name):
    os.mkdir(folder_name + experiment_name)

with open(folder_name + experiment_name + '/' + experiment_name + '.yml', 'w') as file:
    documents = yaml.dump({'date': date, 'experiment_name': experiment_name, 'model_name': model_name, 
                        'catchment_num': catchment_num, 'catchment_valid': catchment_valid, 'catchment_kwargs': catchment_kwargs, 
                        'optimization_kwargs': optimization_kwargs}, file)