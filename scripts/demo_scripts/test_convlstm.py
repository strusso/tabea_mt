import torch
from pathlib import Path
import matplotlib.pyplot as plt
from torch import nn, optim
from sklearn.metrics import r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test
from mlflood.dataset_visualization import visualize_batch, visualize_sample
from mlflood.models.CNN_base import CNN_base
from mlflood.models.unet import UNet
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs
from torchsummary import summary
import numpy as np
import datetime
import time
import copy
from torch.utils.tensorboard import SummaryWriter
from mlflood.evaluation import predict_batch, predict_event, mae_event, predict_dataset, predict_next_ts
from mlflood.evaluation import plot_metric_event, boxplot_metric, multiboxplot, plot_answer_sample, plot_pie, plot_wd_evol, split_values
from mlflood.evaluation import numpy2movie 
from mlflood.training import training_loop
cuda_device = 0
device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")

# Configurations
catchment_num = "709"
border_size = 0
timestep = 1
lead_time = 1
accumulate_rain = False
add_dry_timesteps = None
model_name = "convlstm"
use_diff_dem = False
use_topo_index = True
normalize_output = False
ts_out  = 1
convlstm = 5

# parameter for the catchment
catchment_kwargs = {}
catchment_kwargs["tau"] = 0.5
catchment_kwargs["timestep"]=timestep      # for timestep >1 use CNN rolling or Unet
catchment_kwargs["lead_time"]=lead_time
catchment_kwargs["accumulate_rain"] = accumulate_rain
catchment_kwargs["add_dry_timesteps"] = add_dry_timesteps
catchment_kwargs["sample_type"]="single"
catchment_kwargs["dim_patch"]=256
catchment_kwargs["fix_indexes"]=True
catchment_kwargs["border_size"] = border_size
catchment_kwargs["normalize_output"] = normalize_output
catchment_kwargs["use_diff_dem"] = use_diff_dem
catchment_kwargs["use_topo_index"] = use_topo_index
catchment_kwargs["ts_out"] = ts_out 
catchment_kwargs["convlstm"] = convlstm
catchment_kwargs["precision"]=32

# date
date = "20220630"
# date = datetime.datetime.now().strftime("%Y%m%d")


# test event
event_num = 4

# load test dataset
test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 1)

# Load UNet
# date = datetime.datetime.now().strftime("%Y%m%d")
date = "20220630"
model_name = "convlstm"
experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220630-exp_convlstm_dry/' + experiment_name
model_p = path_loc + "/trained_model.pth"
model = torch.load(model_p)  # Load
model.eval();

index_e = event_num
start_ts = 0
T = len(test_dataset.rainfall_events[index_e]) - start_ts - convlstm 

# waterdepth and rainfall input
wd=test_dataset.waterdepth[index_e][start_ts:]
ground_truth=test_dataset.waterdepth[index_e][convlstm:convlstm+T]
rainfall = test_dataset.rainfall_events[index_e][start_ts:]

# full reconstruction
recons_pred_full = torch.zeros(T, test_dataset.px, test_dataset.py)    

# iterate through patches
for (x_p, y_p) in test_dataset.inds:
    xin, mask, dem, diff_dem, topo_index, _ = test_dataset.crop_to_patch(x_p, y_p, wd, test_dataset.dem_mask, test_dataset.dem, 
    test_dataset.diff_dem, test_dataset.topo_index)

    # initialize convlstm with hidden states = None
    states = None

    for index_t in range(T):
        #varying input
        x_in = xin[index_t: index_t + convlstm]
        X = test_dataset.build_inputs_convlstm(x_in, rainfall, mask, dem, diff_dem, topo_index)
        input = X[0].unsqueeze(0).to(device)
        mask = mask.to(device)
        output = model(input, mask) #, states)
        prediction = output["y_pred"].detach().cpu()
        recons_pred_full[index_t, x_p:x_p+256, y_p:y_p+256] = prediction
        #states = [output["convlstm1_states"], output["convlstm2_states"], output["convlstm3_states"]]


# compute mae for timesteps
mae_convlstm = np.zeros((T))
mae_convlstm[start_ts:start_ts+T]= mae_event(recons_pred_full, ground_truth, test_dataset.dem_mask)

# compute mae boxplots
# initialize limits for boxplots of different water depths
lims = [0.1, 0.2, 0.5, 1]
# initialize lists for boxplot maes
mae_convlstm_boxplot = boxplot_metric(recons_pred_full, ground_truth, test_dataset.dem_mask, lims, pred_ts = None, metric="mae")

# save model outputs and metrics
save_path = path_loc + "/event_" +str(index_e) + "/"
# save full output
# np.save(save_path + "full_pred", recons_pred_full)
np.save(save_path + "mae", mae_convlstm)
np.save(save_path + "mae_boxplot", mae_convlstm_boxplot, allow_pickle=True)