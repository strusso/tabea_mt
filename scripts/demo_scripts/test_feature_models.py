#imports
from re import A
from tracemalloc import start
import torch
from pathlib import Path
import matplotlib.pyplot as plt
from torch import nn, optim
from sklearn.metrics import label_ranking_loss, r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test
from mlflood.dataset_visualization import visualize_batch, visualize_sample
from mlflood.models.CNN_base import CNN_base
from mlflood.models.unet import UNet
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs
from torchsummary import summary
import numpy as np
import datetime
import time
import copy
from torch.utils.tensorboard import SummaryWriter
from mlflood.evaluation import plot_metric_event, predict_batch, predict_event, mae_event, nse_event, mse_event, predict_dataset, predict_next_ts
from mlflood.evaluation import plot_metric_event, multiboxplot, plot_answer_sample, boxplot_metric, plot_pie, plot_wd_evol, split_values
from mlflood.evaluation import numpy2movie 
from mlflood.training import training_loop
import pandas as pd

# configurations
cuda_device = 0
device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")
catchment_num = "709"
border_size = 0
timestep = 1
accumulate_rain = False
model_name = "unet"
use_diff_dem = False
use_topo_index = True
normalize_output = False
ts_out  = 1
lead_time = 1
#date = datetime.datetime.now().strftime("%Y%m%d")
date = "20220527"

catchment_kwargs = {}
catchment_kwargs["tau"] = 0.5
catchment_kwargs["timestep"]=timestep      # for timestep >1 use CNN rolling or Unet
catchment_kwargs["accumulate_rain"] = accumulate_rain
catchment_kwargs["sample_type"]="single"
catchment_kwargs["dim_patch"]=256
catchment_kwargs["fix_indexes"]=True
catchment_kwargs["border_size"] = border_size
catchment_kwargs["normalize_output"] = normalize_output
catchment_kwargs["use_diff_dem"] = use_diff_dem
catchment_kwargs["use_topo_index"] = use_topo_index
catchment_kwargs["ts_out"] = ts_out 

# optimization paramters
optimization_kwargs = {}
optimization_kwargs["batch_size"] = 8
optimization_kwargs["epochs"] = 100
optimization_kwargs["learning_rate"] = 0.0001
optimization_kwargs["weight_d"] = 0.0001 

# initialize limits for boxplots of different water depths
lims = [0.1, 0.2, 0.5, 1]

# rainfall event
event_num = 4

def f_mae(use_diff_dem, use_topo_index):
    # predict event with model lead time 1 in ar mode
    catchment_kwargs["use_diff_dem"] = use_diff_dem
    catchment_kwargs["use_topo_index"] = use_topo_index

    # load test dataset
    test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
    dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 8)
    print('--------------------------------------')
    print('use diff dem: ', use_diff_dem)
    print('use topo: ', use_topo_index)
    print('channels: ', test_dataset.input_channels)

    # Load UNet
    # date = "20220515"
    experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
    path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220527-exp_features/' + experiment_name
    model_p = path_loc + "/trained_model.pth"
    model = torch.load(model_p)  # Load
    model.eval();

    # predict event
    pred_event, gt_event, mask = predict_event(model, test_dataset, event_num, start_ts=0, ar = False, ts_out = ts_out)
    mae_array = mae_event(pred_event, gt_event, mask)
        
    # compute boxplot
    mae_boxplot_list = boxplot_metric(pred_event, gt_event, mask, lims, metric = "mae")

    # plot sample
    folder = '/scratch/tdonauer/tabea_mt/plots/predict_features/wd_samples/'
    #plot_answer_sample(pred_event, gt_event, mask, ts=8, zoom = None, show_diff=False, global_scale=False, save_folder = folder, model_name = experiment_name, start_ts=None, lead_time=None, ar=False)

    '''
    # plot high waterdepth error
    pred50 = pred_event[50]
    gt50 = gt_event[50] 
    high_wd = np.where(gt50>0.5, True, 0)
    high_error = np.where(high_wd, (pred50-gt50), 0)
    high_error = np.where(mask, high_error, np.nan)
    #diff dem to compare
    diff_dem = test_dataset.diff_dem
    diff_mean = np.mean(np.array(diff_dem), axis=0)
    diff_mean = np.where(mask, diff_mean, np.nan)
    fig, axs = plt.subplots(1,2, figsize=(14,5))
    pos0=axs[0].imshow(high_error, vmin=-0.05, vmax=0.05)
    pos1=axs[1].imshow(diff_mean, vmin=-0.0005, vmax=0.0005)
    axs[0].set_title('Waterdepth>0.5 m: Pred-GT',fontweight="bold")
    axs[1].set_title('Mean: Diff_DEM',fontweight="bold")
    cbar0 = fig.colorbar(pos0, ax=axs[0], extend='both', fraction=0.136, pad=0.02)
    cbar1 = fig.colorbar(pos1, ax=axs[1], extend='both', fraction=0.136, pad=0.02)
    plt.savefig(folder+experiment_name+'high_wd', dpi=1200)
    '''
        
    # clean up memory
    del test_dataset, dataloaders_test, model
    return mae_array, mae_boxplot_list


#function calls for each model
mae_unet, mae_unet_boxplot = f_mae(use_diff_dem=False, use_topo_index=False)
mae_topo, mae_topo_boxplot = f_mae(use_diff_dem=False, use_topo_index=True)
mae_diff_dem, mae_diff_dem_boxplot = f_mae(use_diff_dem=True, use_topo_index=False)
mae_topo_diff_dem, mae_topo_diff_boxplot = f_mae(use_diff_dem=True, use_topo_index=True)

# Boxplots 
boxplots=[mae_unet_boxplot, mae_topo_boxplot, mae_topo_diff_boxplot, mae_diff_dem_boxplot]
models=['DEM', 'DEM + Topo_Index', 'DEM + Topo_Index + Diff_DEM', 'DEM + Diff_DEM']
bins = ['0-10cm', '10-20cm', '20-50cm', '50-100cm', '>100cm']
colors = ['#EF8A62', '#67A9CF', '#1B9E77', '#CA0020', '#998EC3']

# sanity check: boxplot data should have the same length
folder = '/scratch/tdonauer/tabea_mt/plots/predict_features/event_4/'
multiboxplot(boxplots, bins, models, colors, save_folder=folder, metric_label='MAE [m]', name='feature_models_MAE', title = None)
print('boxplot figure created')

''' create time series plots '''
# delete boxplot data
del boxplots, mae_unet_boxplot, mae_topo_boxplot, mae_topo_diff_boxplot, mae_diff_dem_boxplot 
test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
rainfall_timeseries = test_dataset.rainfall_events[event_num]

# choose the respective row of the mae array
maes=[mae_unet, mae_topo, mae_topo_diff_dem, mae_diff_dem]

for mae in maes:    
    print(np.mean(mae))

name='compare_mae_features'
title = None
plot_metric_event(maes, models, colors, start_ts=0, metric_label= 'Mean absolute error [m]', save_folder=folder, rainfall_series=rainfall_timeseries, name=name,title=title)
