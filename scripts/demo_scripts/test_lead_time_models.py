#imports
from re import A
from tracemalloc import start
import torch
from pathlib import Path
import matplotlib.pyplot as plt
from torch import nn, optim
from sklearn.metrics import label_ranking_loss, r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test, MyCatchment
from mlflood.conf import PATH_GENERATED, PATH_709
from mlflood.dataset_visualization import visualize_batch, visualize_sample
from mlflood.models.CNN_base import CNN_base
from mlflood.models.unet import UNet
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs
from torchsummary import summary
import numpy as np
import datetime
import time
import copy
from torch.utils.tensorboard import SummaryWriter
from mlflood.evaluation import plot_metric_event, predict_batch, predict_event, mae_event, predict_dataset, predict_next_ts
from mlflood.evaluation import multiboxplot, plot_answer_sample, boxplot_metric, plot_pie, plot_wd_evol, split_values
from mlflood.evaluation import numpy2movie 
from mlflood.training import training_loop
import pandas as pd

# configurations
cuda_device = 0
device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")
catchment_num = "709"
border_size = 0
timestep = 1
accumulate_rain = False
model_name = "unet"
use_diff_dem = False
use_topo_index = True
normalize_output = False
ts_out  = 1
date = datetime.datetime.now().strftime("%Y%m%d")

catchment_kwargs = {}
catchment_kwargs["tau"] = 0.5
catchment_kwargs["timestep"]=timestep      # for timestep >1 use CNN rolling or Unet
catchment_kwargs["accumulate_rain"] = accumulate_rain
catchment_kwargs["sample_type"]="single"
catchment_kwargs["dim_patch"]=256
catchment_kwargs["fix_indexes"]=True
catchment_kwargs["border_size"] = border_size
catchment_kwargs["normalize_output"] = normalize_output
catchment_kwargs["use_diff_dem"] = use_diff_dem
catchment_kwargs["use_topo_index"] = use_topo_index
catchment_kwargs["ts_out"] = ts_out 
catchment_kwargs["add_dry_timesteps"] = None

# optimization paramters
optimization_kwargs = {}
optimization_kwargs["batch_size"] = 8
optimization_kwargs["epochs"] = 100
optimization_kwargs["learning_rate"] = 0.0001
optimization_kwargs["weight_d"] = 0.0001 

# initialize limits for boxplots of different water depths
lims = [0.1, 0.2, 0.5, 1]



def boxplot_to_pandas(boxplot_list, model_name, bins):
    df_boxplot = pd.DataFrame(columns=['bin', 'wd_error', 'model'])
    for i, bin in enumerate(bins):
        df_tmp= pd.DataFrame({'wd_error':boxplot_list[i][:], 'bin': bin, 'model': model_name})
        df_boxplot = pd.concat([df_boxplot, df_tmp])
    return df_boxplot

def f_mae_ar(mae_ar, mae_ar_boxplot, start_timesteps):
    # predict event with model lead time 1 in ar mode
    lead_time = 1
    catchment_kwargs["lead_time"] = lead_time

    # load test dataset
    test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
    dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 8)

    # Load UNet
    date = "20220530"
    model_name = "unet"
    experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
    path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220530-exp_unet/' + experiment_name
    model_p = path_loc + "/trained_model.pth"
    model = torch.load(model_p)  # Load
    model.eval();

    # AR: predict for start timesteps 0 to 5
    for start_ts in start_timesteps:
        pred_event, gt_event, mask = predict_event(model, test_dataset, event_num, start_ts, ar = True, ts_out = ts_out)
        mae_ar[start_ts, start_ts:start_ts+12]= mae_event(pred_event, gt_event, mask)[0:12]
        
        # compute boxplot
        for lead_time in range(1,13):
            splits = split_values(pred_event[lead_time-1][mask], gt_event[lead_time-1][mask], lims=lims);
            boxplot_ar = [np.abs(split[0]-split[1]).flatten() for split in splits]
            for bin in range(len(lims)+1):
                mae_ar_boxplot[bin] = np.concatenate((boxplot_ar[bin], mae_ar_boxplot[bin]))
            
            if ((lead_time==6) or (lead_time==12)) and (start_ts==2):
                folder = '/scratch/tdonauer/tabea_mt/plots/wd_samples/'
                plot_answer_sample(pred_event[lead_time-1], gt_event[lead_time-1], mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'UNet: AR', start_ts=start_ts, lead_time=lead_time, ar=False)


    # clean up memory
    del test_dataset, dataloaders_test, model
    return mae_ar, mae_ar_boxplot

def f_mae_direct(mae_direct_unet, mae_direct_cnn, mae_direct_unet_boxplot, mae_direct_cnn_boxplot, start_timesteps):
    # predict with directly trained CNN and unet
    for lead_time in range(1, 13):
        catchment_kwargs["lead_time"] = lead_time

        # load test dataset with correct rainfall depending on lead time
        test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
        dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 8)

        # Load CNN
        date = "20220512"
        model_name = "cnn"
        experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
        path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220512-exp_cnn/' + experiment_name
        model_p = path_loc + "/trained_model.pth"
        model = torch.load(model_p)  
        model.eval();

        # Load Unet
        date = "20220530"
        model_name = 'unet'
        experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
        path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220530-exp_unet/' + experiment_name
        model_p = path_loc + "/trained_model.pth"
        unet = torch.load(model_p)  
        unet.eval();
        
        # predict for start timesteps 0 to 11
        for start_ts in start_timesteps:   
            T = len(test_dataset.rainfall_events[event_num]) - test_dataset.lead_time - start_ts - 1     
            
            # predict_next_ts
            xin = test_dataset.waterdepth[event_num][start_ts : start_ts + timestep]
            rainfall = test_dataset.rainfall_events[event_num][start_ts:]
            pred_cnn_direct = predict_next_ts(test_dataset, model, xin, rainfall, test_dataset.dem_mask, test_dataset.dem, test_dataset.diff_dem, test_dataset.topo_index, ts_out)
            ground_truth = test_dataset.waterdepth[event_num][-T , : , :]
            mask = test_dataset.dem_mask

            # compute mae for respective ts and start ts
            pred = pred_cnn_direct[mask]
            gt = ground_truth[mask]
            mae_direct_cnn[start_ts, start_ts+lead_time-1] = np.mean(np.array(abs(pred-gt)))

            pred_unet_direct = predict_next_ts(test_dataset, unet, xin, rainfall, test_dataset.dem_mask, test_dataset.dem, test_dataset.diff_dem, test_dataset.topo_index, ts_out)
            pred_unet = pred_unet_direct[mask]
            mae_direct_unet[start_ts, start_ts+lead_time-1] = np.mean(np.array(abs(pred_unet-gt)))

            # compute boxplots for cnn
            splits = split_values(pred, gt, lims=lims);
            boxplot_cnn_direct = [np.abs(split[0]-split[1]).flatten() for split in splits]
            for bin in range(len(lims)+1):
                mae_direct_cnn_boxplot[bin] = np.concatenate((boxplot_cnn_direct[bin], mae_direct_cnn_boxplot[bin]))

            # compute boxplots for unet
            splits = split_values(pred_unet, gt, lims=lims);
            boxplot_unet_direct = [np.abs(split[0]-split[1]).flatten() for split in splits]
            for bin in range(len(lims)+1):
                mae_direct_unet_boxplot[bin] = np.concatenate((boxplot_unet_direct[bin], mae_direct_unet_boxplot[bin]))

            if ((lead_time==6) or (lead_time==12)) and (start_ts == 2):
                folder = '/scratch/tdonauer/tabea_mt/plots/wd_samples/'
                plot_answer_sample(pred_unet_direct, ground_truth, mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'UNet: Direct', start_ts=start_ts, lead_time=lead_time, ar=False)
                plot_answer_sample(pred_cnn_direct, ground_truth, mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'CNN: Direct', start_ts=start_ts, lead_time=lead_time, ar=False)

        # clean up memory 
        del test_dataset, dataloaders_test, model
    return mae_direct_unet, mae_direct_cnn, mae_direct_unet_boxplot, mae_direct_cnn_boxplot

def f_mae_unet_rain(mae_direct_unet_rain, mae_unet_rain_boxplot, mae_unet_dry_ts, mae_unet_dry_ts_boxplot, start_timesteps):
    # predict with directly trained CNN and unet
    for lead_time in range(1, 13):
        print('lead time:', lead_time)
        accumulate_rain = False
        catchment_kwargs["lead_time"] = lead_time
        catchment_kwargs["accumulate_rain"] = accumulate_rain

        # load test dataset with correct rainfall depending on lead time
        test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
        dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 8)
        #test_dataset = MyCatchment(PATH_GENERATED / Path("F_01_real-002.h5"), catchment_num="F01", **catchment_kwargs)
        #dataloaders_test = dataloader_test(test_dataset, batch_size = 8)

        # Load Unet with rainfall
        date = "20220518"
        model_name = 'unet'
        experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
        path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220518-exp_unet/'  + experiment_name
        model_p = path_loc + "/trained_model.pth"
        unet = torch.load(model_p)
        unet.eval()
        
        # predict for start timesteps 0 to 11
        for start_ts in start_timesteps:   
            T = len(test_dataset.rainfall_events[event_num]) - test_dataset.lead_time - start_ts - 1     
            
            # predict_next_ts
            xin = test_dataset.waterdepth[event_num][start_ts : start_ts + timestep]
            rainfall = test_dataset.rainfall_events[event_num][start_ts:]
            pred_unet_rain = predict_next_ts(test_dataset, unet, xin, rainfall, test_dataset.dem_mask, test_dataset.dem, test_dataset.diff_dem, test_dataset.topo_index, ts_out)
            ground_truth = test_dataset.waterdepth[event_num][-T , : , :]
            mask = test_dataset.dem_mask

            # mask and compute mae
            gt = ground_truth[mask]
            pred_unet = pred_unet_rain[mask]
            mae_direct_unet_rain[start_ts, start_ts+lead_time-1] = np.mean(np.array(abs(pred_unet-gt)))

            # compute boxplot for different water depth values
            splits = split_values(pred_unet, gt, lims=lims);
            boxplot_unet_rain = [np.abs(split[0]-split[1]).flatten() for split in splits];
            for bin in range(len(lims)+1):
                mae_unet_rain_boxplot[bin] = np.concatenate((boxplot_unet_rain[bin], mae_unet_rain_boxplot[bin]))

            if ((lead_time==6) or (lead_time==12)) and (start_ts in [5, 20, 50, 80]):
                folder = '/scratch/tdonauer/tabea_mt/plots/wd_samples/'
                plot_answer_sample(pred_unet_rain, ground_truth, mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'UNet: Direct (b)', start_ts=start_ts, lead_time=lead_time, ar=False)
        
        # Load Unet with rainfall and dry timestep
        date = "20220530"
        model_name = 'unet'
        experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
        path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/20220530-exp_unet/' + experiment_name
        model_dry = path_loc + "/trained_model.pth"
        unet = torch.load(model_dry)
        unet.eval()
        
        # predict for start timesteps 0 to 11
        for start_ts in start_timesteps:   
            T = len(test_dataset.rainfall_events[event_num]) - test_dataset.lead_time - start_ts - 1     
            
            # predict_next_ts
            xin = test_dataset.waterdepth[event_num][start_ts : start_ts + timestep]
            rainfall = test_dataset.rainfall_events[event_num][start_ts:]
            pred_unet_dry_ts = predict_next_ts(test_dataset, unet, xin, rainfall, test_dataset.dem_mask, test_dataset.dem, test_dataset.diff_dem, test_dataset.topo_index, ts_out)
            ground_truth = test_dataset.waterdepth[event_num][-T , : , :]
            mask = test_dataset.dem_mask

            # mask and compute mae
            gt = ground_truth[mask]
            pred_unet = pred_unet_dry_ts[mask]
            mae_unet_dry_ts[start_ts, start_ts+lead_time-1] = np.mean(np.array(abs(pred_unet-gt)))

            # compute boxplot for different water depth values
            splits = split_values(pred_unet, gt, lims=lims);
            boxplot_unet_dry = [np.abs(split[0]-split[1]).flatten() for split in splits];
            for bin in range(len(lims)+1):
                mae_unet_dry_ts_boxplot[bin] = np.concatenate((boxplot_unet_dry[bin], mae_unet_dry_ts_boxplot[bin]))

            if ((lead_time==6) or (lead_time==12)) and (start_ts in [5, 20, 50, 80]):
                folder = '/scratch/tdonauer/tabea_mt/plots/wd_samples/'
                #plot_answer_sample(np.where(mask, pred_unet_dry_ts, 0), np.where(mask, ground_truth, 0), mask, ts=None, zoom = None, show_diff=False, global_scale=False, save_folder = folder, model_name = 'UNet: Dry timesteps', start_ts=start_ts, lead_time=lead_time, ar=False)
                plot_answer_sample(pred_unet_dry_ts, ground_truth, mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'UNet: Direct (c)', start_ts=start_ts, lead_time=lead_time, ar=False)

        # clean up memory 
        # del test_dataset, dataloaders_test, unet
    return mae_direct_unet_rain, mae_unet_rain_boxplot, mae_unet_dry_ts, mae_unet_dry_ts_boxplot


def f_mae_baseline(mae_baseline, mae_baseline_boxplot, start_timesteps):
    test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
    
    for lead_time in range(1,13):

        # predict for start timesteps 0 to 5
        for start_ts in start_timesteps:   # compute baseline mae
            T = len(test_dataset.rainfall_events[event_num]) - lead_time - start_ts - 1     
            pred_baseline = test_dataset.waterdepth[event_num][start_ts]
            ground_truth = test_dataset.waterdepth[event_num][-T , : , :]
            mask = test_dataset.dem_mask

            # compute mae for respective ts and start ts
            pred = pred_baseline[mask]
            gt = ground_truth[mask]
            mae_baseline[start_ts, start_ts+lead_time-1] = np.mean(np.array(abs(pred-gt)))
            
            # compute boxplot for different water depth values
            splits = split_values(pred, gt, lims=lims);
            boxplot_mae = [np.abs(split[0]-split[1]).flatten() for split in splits]
            for bin in range(len(lims)+1):
                mae_baseline_boxplot[bin] = np.concatenate((boxplot_mae[bin], mae_baseline_boxplot[bin]))
            
            if ((lead_time==6) or (lead_time==12)) and (start_ts == 2):
                folder = '/scratch/tdonauer/tabea_mt/plots/wd_samples/'
                plot_answer_sample(pred_baseline, ground_truth, mask, ts=None, zoom = [500,1000,500,1000], show_diff=False, global_scale=False, save_folder = folder, model_name = 'Baseline', start_ts=start_ts, lead_time=lead_time, ar=False)

    del test_dataset
    return mae_baseline, mae_baseline_boxplot

"""Initialize arrays and list to store mae values"""
# rainfall event
test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
dataloaders_test = dataloader_test(test_dataset, catchment_num = "709", batch_size = 8)

#test_dataset = MyCatchment(PATH_GENERATED / Path("F_01_real-002.h5"), catchment_num="F01", **catchment_kwargs)
#dataloaders_test = dataloader_test(test_dataset, batch_size = 8)
#event_num = 1
#start_timesteps = np.concatenate(([0, 5], np.arange(10, 140, 10)))

# for catchment 709
event_num = 0
start_timesteps = np.concatenate((np.arange(0, 6), np.arange(12, 30, 6)))
rainfall_timeseries = test_dataset.rainfall_events[event_num]
del test_dataset


# arrays for mae values
mae_ar, mae_direct_unet, mae_direct_cnn, mae_direct_unet_rain, mae_unet_dry_ts, mae_baseline = np.zeros((25,36)), np.zeros((25,36)), np.zeros((25,36)), np.zeros((25,36)), np.zeros((25,36)), np.zeros((25,36))
# mae_direct_unet_rain, mae_unet_dry_ts = np.zeros((len(event), len(event))), np.zeros((len(event), len(event)))
# mae_direct_unet_rain, mae_unet_dry_ts = np.zeros((24,36)), np.zeros((24,36))

# initialize lists for boxplot maes
mae_ar_boxplot, mae_direct_unet_boxplot, mae_direct_cnn_boxplot, mae_unet_rain_boxplot, mae_unet_dry_ts_boxplot, mae_baseline_boxplot = [], [], [], [], [], []
# mae_unet_rain_boxplot, mae_unet_dry_ts_boxplot = [], []


for i in range(len(lims)+1):
    mae_ar_boxplot.append(torch.zeros(0))
    mae_direct_cnn_boxplot.append(torch.zeros(0))
    mae_direct_unet_boxplot.append(torch.zeros(0))
    mae_unet_rain_boxplot.append(torch.zeros(0))
    mae_unet_dry_ts_boxplot.append(torch.zeros(0))
    mae_baseline_boxplot.append(torch.zeros(0))


#function calls for each model
mae_direct_unet, mae_direct_cnn, mae_direct_unet_boxplot, mae_direct_cnn_boxplot = f_mae_direct(mae_direct_unet, mae_direct_cnn, mae_direct_unet_boxplot, mae_direct_cnn_boxplot, start_timesteps)
mae_direct_unet_rain, mae_unet_rain_boxplot, mae_unet_dry_ts, mae_unet_dry_ts_boxplot = f_mae_unet_rain(mae_direct_unet_rain, mae_unet_rain_boxplot, mae_unet_dry_ts, mae_unet_dry_ts_boxplot, start_timesteps)
mae_baseline, mae_baseline_boxplot = f_mae_baseline(mae_baseline, mae_baseline_boxplot, start_timesteps)
mae_ar, mae_ar_boxplot = f_mae_ar(mae_ar, mae_ar_boxplot, start_timesteps)
print('boxplots computed')

# Boxplots 
boxplots=[mae_ar_boxplot, mae_direct_unet_boxplot, mae_unet_rain_boxplot, mae_unet_dry_ts_boxplot, mae_baseline_boxplot]
models=['UNet: AR', 'UNET: Direct (a)', 'UNet: Direct (b)', 'UNet: Direct (c)', 'Baseline']
bins = ['0-10cm', '10-20cm', '20-50cm', '50-100cm', '>100cm']
colors = ['#EF8A62', '#67A9CF', '#1B9E77', '#CA0020', '#998EC3']

# sanity check: boxplot data should have the same length
# print('len ar boxplot', sum([len(bin) for bin in mae_ar_boxplot]))
# print('len baseline boxplot', sum([len(bin) for bin in mae_baseline_boxplot]))
# print('len cnn direct boxplot', sum([len(bin) for bin in mae_direct_cnn_boxplot]))
# print('len unet direct boxplot', sum([len(bin) for bin in mae_ar_boxplot]))
print('len unet rain boxplot', sum([len(bin) for bin in mae_unet_rain_boxplot]))
multiboxplot(boxplots, bins, models, colors, save_folder='/scratch/tdonauer/tabea_mt/plots/', name='dry_ts_real_event', title = None)
print('boxplot figure created')


''' create time series plots '''
# delete boxplot data
del mae_baseline_boxplot, mae_ar_boxplot, mae_direct_cnn_boxplot, mae_direct_unet_boxplot, mae_unet_rain_boxplot, mae_unet_dry_ts_boxplot

# change zeros to nan in mae arrays
# mae_arrays = [mae_direct_cnn, mae_ar, mae_direct_unet, mae_direct_unet_rain, mae_baseline]
mae_arrays = [mae_ar, mae_direct_unet, mae_direct_unet_rain, mae_unet_dry_ts, mae_baseline]

for array in mae_arrays:
    array[array < 1e-20] = np.nan
    

for start_ts in start_timesteps:
    print('timeseries plot')
    # choose the respective row of the mae array
    # maes=[mae_direct_cnn[start_ts], mae_ar[start_ts], mae_direct_unet[start_ts], mae_direct_unet_rain[start_ts], mae_baseline[start_ts]]
    maes=[mae_ar[start_ts], mae_direct_unet[start_ts], mae_direct_unet_rain[start_ts], mae_unet_dry_ts[start_ts], mae_baseline[start_ts]]
    name='compare_maes_lead_time_1-12_start_time_'+ str(start_ts)
    title = 'Start timestep: ' + str(start_ts)
    folder = '/scratch/tdonauer/tabea_mt/plots/predict_lead_time/event_0/unets/'
    plot_metric_event(maes, models, colors, start_ts, 'Mean absolute error [m]', folder, rainfall_timeseries, name, title)

''' 
create multiboxplot with seaborn
df_baseline = boxplot_to_pandas(mae_baseline_boxplot, 'Baseline', bins)
df_unet_rain = boxplot_to_pandas(mae_unet_rain_boxplot, 'UNet: Rain', bins)
df_unet_direct = boxplot_to_pandas(mae_direct_unet_boxplot, 'UNet: Direct', bins)
df_cnn_direct = boxplot_to_pandas(mae_direct_cnn_boxplot, 'CNN: Direct', bins)
df_ar = boxplot_to_pandas(mae_ar_boxplot, 'UNet: AR', bins)

df_boxplots = pd.concat([df_baseline, df_unet_rain, df_ar, df_unet_direct, df_cnn_direct])

ax = sns.boxplot(x='bin', y='wd_error', hue='model', data = df_boxplots, showfliers = False)
ax.set_xlabel("Water depth", fontsize = 20)
ax.set_ylabel("Absolute error [m]", fontsize = 20)
plt.savefig('/cluster/scratch/tdonauer/tabea_mt/plots/seaborn_baseline.png', dpi=1200)   # save the figure to file
'''