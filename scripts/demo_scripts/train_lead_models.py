# imports
import torch
from pathlib import Path
import matplotlib.pyplot as plt
from torch import nn, optim
from sklearn.metrics import r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test
from mlflood.dataset_visualization import visualize_batch, visualize_sample
from mlflood.models.CNN_base import CNN_base
from mlflood.models.unet import UNet
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs
from torchsummary import summary
import numpy as np
import datetime
import time
import copy
from torch.utils.tensorboard import SummaryWriter
from mlflood.evaluation import predict_batch, predict_event, mae_event, predict_dataset, predict_next_ts
from mlflood.evaluation import plot_metric_event, multiboxplot, plot_answer_sample, boxplot_mae, plot_pie, plot_wd_evol
from mlflood.evaluation import numpy2movie 
from mlflood.training import training_loop

cuda_device = 0
device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")

#configurations
catchment_num = "709"
border_size = 0
timestep = 1
accumulate_rain = False
add_dry_timesteps = 12
model_name = "unet"
use_diff_dem = False
use_topo_index = True
normalize_output = False
ts_out  = 1
date = datetime.datetime.now().strftime("%Y%m%d")
#date = "20220517"

catchment_kwargs = {}
catchment_kwargs["tau"] = 0.5
catchment_kwargs["timestep"]=timestep      # for timestep >1 use CNN rolling or Unet
catchment_kwargs["accumulate_rain"] = accumulate_rain
catchment_kwargs["add_dry_timesteps"] = add_dry_timesteps
catchment_kwargs["sample_type"]="single"
catchment_kwargs["dim_patch"]=256
catchment_kwargs["fix_indexes"]=True
catchment_kwargs["border_size"] = border_size
catchment_kwargs["normalize_output"] = normalize_output
catchment_kwargs["use_diff_dem"] = use_diff_dem
catchment_kwargs["use_topo_index"] = use_topo_index
catchment_kwargs["ts_out"] = ts_out 

# optimization paramters
optimization_kwargs = {}
optimization_kwargs["batch_size"] = 8
optimization_kwargs["epochs"] = 20
optimization_kwargs["learning_rate"] = 0.0001
optimization_kwargs["weight_d"] = 0.0001 


for lead_time in range(1,13):
    
    # specify directory
    catchment_kwargs["lead_time"]=lead_time
    experiment_name = f"{date}-{catchment_num}-{model_name}-{lead_time}-{accumulate_rain}-{use_diff_dem}-{use_topo_index}"
    path_loc = '/scratch/tdonauer/tabea_mt/data/709_experiments/' + experiment_name

    # create dataset
    train_dataset, valid_dataset = load_dataset(catchment_num=catchment_num, **catchment_kwargs)
    dataloaders_train, dataloaders_valid = dataloader_tr_val(train_dataset, valid_dataset, catchment_num = "709", batch_size = 8)
    n_input_channels = train_dataset.input_channels

    # load model
    if model_name=="cnn":
        model = CNN_base(border_size=border_size, timestep = timestep, n_input_channels=n_input_channels, use_diff_dem=use_diff_dem, use_topo_index=use_topo_index, ts_out = ts_out)
    elif model_name == "unet":
        model = UNet(border_size=border_size, timestep = timestep, n_input_channels=n_input_channels, use_diff_dem=use_diff_dem, use_topo_index=use_topo_index)
    model = model.to(device)

    # train the model
    model, history = training_loop(model, l1_loss, dataloaders_train, dataloaders_valid, optimization_kwargs, device, path_loc)

    # delete the datasets
    del train_dataset, valid_dataset, dataloaders_train, dataloaders_valid, model, history

print('Done')



