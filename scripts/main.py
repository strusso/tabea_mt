    
"""
This code was used to train and test a deep learning model to predict urban pluvial floods
in the scope of my master thesis (Tabea, 2022).
To work in a reproducible way, the script was run with yml-configuration files.
The necessary mlflood package can be found on gitlab: https://gitlab.ethz.ch/strusso/tabea_mt
"""
    
# general imports
import argparse, os
import numpy as np
import matplotlib as plt
import time
import yaml
import csv

# import torch functionalities
import torch
import torch.nn.functional as F
from torch.utils.tensorboard import SummaryWriter
from torchsummary import summary

# import project specific functions and deep learning models
from mlflood.dataset import load_dataset, dataloader_tr_val, load_test_dataset
from mlflood.models.simvp import SimVP
from mlflood.models.unet import UNet
from mlflood.models.swin_unet.SwinUNet3D import SwinUNet3D
from mlflood.models.convlstm import convlstm_seq
from mlflood.training import training_loop
from mlflood.evaluation import predict_event, mae_event, recall_event, precision_event, plot_answer_sample

# functions 
def train(args):
    """
    This function trains a deep learning model for urban flood prediction based on a configuration file yml.
    The trained model is stored in the specified save_dir, otherwise there is no return value.
    """

    # specify directory
    path_loc = args.save_dir 

    # create dataset
    train_dataset, valid_dataset = load_dataset(catchment_num=catchment_num, catchment_valid=catchment_num, **catchment_kwargs)
    dataloaders_train, dataloaders_valid = dataloader_tr_val(train_dataset, valid_dataset, 
        catchment_num = catchment_num, batch_size=optimization_kwargs['batch_size'])
    n_input_channels = train_dataset.input_channels

    # initialize optimization parameters
    start_epoch = 1
    best_loss = np.Inf

    # if there is a pretrained model in the directory, load the necessary variables for continue training
    if os.path.exists(path_loc + "/checkpoint.pth"):
        print("Continue training")
        checkpoint = torch.load(path_loc + "/checkpoint.pth")
        model = checkpoint["model"]
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
        start_epoch = checkpoint['epoch'] + 1
        model.load_state_dict(checkpoint['model_state'])
        optimizer.load_state_dict(checkpoint['optimizer_state'])
        best_loss = checkpoint['best_loss']

    # else initialise a new model object (depending on the model name)
    elif model_name == "unet":
        model = UNet(border_size=catchment_kwargs['border_size'], timestep=catchment_kwargs['timestep'], n_input_channels = n_input_channels, 
        use_diff_dem = catchment_kwargs['use_diff_dem'], use_topo_index=catchment_kwargs['use_topo_index'], ts_out=catchment_kwargs['ts_out'])
    elif model_name == "convlstm":
        model = convlstm_seq(num_channels=n_input_channels)
    elif model_name == "simvp":
        model = SimVP(shape_in=[catchment_kwargs['convlstm'], n_input_channels, catchment_kwargs['dim_patch'], catchment_kwargs['dim_patch']])
    elif model_name == "swinunet":
        model = SwinUNet3D(in_chans=n_input_channels, out_chans=1, in_depth=catchment_kwargs['convlstm'], out_depth=catchment_kwargs['ts_out'])
    
    # define optimizer
    optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
   
    # send model to GPU
    model = model.to(args.device)
        
    # train the model
    model, history = training_loop(model, optimizer, dataloaders_train, dataloaders_valid, 
    optimization_kwargs, args.device, path_loc, start_epoch, best_loss)


def test(args):
    """
    This function tests a deep learning model for urban flood prediction based on a configuration file yml.
    It computes mperformance metrics by comparing the model output to the model target, performance
    metrics are then written to the save_dir. Qualitative maps are also generated and stored.
    """

    # load test dataset 
    test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
    
    # load trained model
    path_loc = args.save_dir 
    checkpoint = torch.load(path_loc + "/checkpoint.pth")
    model = checkpoint["model"]
    model.eval()

    # run model for each event in test dataset
    for event_num in range(len((test_dataset).rainfall_events)):
    
        # predict event, write the required prediction time to file
        start_time=time.time()
        pred_event, gt_event, mask = predict_event(model, test_dataset, event_num, 
        start_ts=None, ar = False, ts_out = catchment_kwargs["ts_out"], convlstm=catchment_kwargs["convlstm"])
        
        prediction_time = time.time() - start_time
        with open(args.save_dir + "results/prediction_time.csv", "a", encoding="UTF-8", newline="") as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([model_name, event_num, prediction_time])

        # compute performance metrics for each predicted flood event
        mae_array = mae_event(pred_event, gt_event, mask)
        precision = precision_event(pred_event, gt_event, mask, thr=np.ones(len(mae_array))*0.5)
        recall = recall_event(pred_event, gt_event, mask, thr=np.ones(len(mae_array))*0.5)

        # save model outputs and metrics
        save_path = path_loc + "/test-" + catchment_num + "/event" + event_num + "/"
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        np.save(save_path + "mae.npy", mae_array)
        np.save(save_path + "precision_fix.npy", precision)
        np.save(save_path + "recall_fix.npy", recall)        
        print("Metrics stored")

        # plot sample images and store them
        for start_timestep in [0, 5, 10, 20]:
            plot_answer_sample(pred_event, gt_event, mask, ts=start_timestep, zoom=None, save_folder=save_path, 
            model_name=model_name, start_ts=None, lead_time=catchment_kwargs["lead_time"], ar=False)
        
        print('Done with event: ', event_num)
        
        # close all open plots
        plt.close('all')

# main
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="training script")

    # general parameters
    parser.add_argument("--device",default="cuda",type=str,choices=["cuda", "cpu"])
    parser.add_argument("--save_dir",default="experiments/20220816-exp_unet/", type=str,  
    help="Path to directory where models and logs should be saved")
    parser.add_argument("--config", default='experiments/20220816-exp_unet/unet-6-False-False-True.yml', type=str, 
                        help="path to experiment configurations saved in yml file")
    args = parser.parse_args()
    
    # read configurations from yml files
    with open(args.config) as file:
        config = yaml.full_load(file)
        catchment_kwargs = config['catchment_kwargs']
        catchment_num = config['catchment_num']
        optimization_kwargs = config['optimization_kwargs']
        model_name = config['model_name']
    
    # model training
    train(args)

    # model testing
    test(args)
