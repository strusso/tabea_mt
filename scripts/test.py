import sys, argparse, os
from imageio import save
import numpy as np
import yaml
import torch
import time
import csv
import matplotlib.pyplot as plt
from torch.utils.tensorboard import SummaryWriter
from mlflood.dataset import load_test_dataset, dataloader_test, load_dataset, dataloader_tr_val
import mlflood.evaluation
from mlflood.evaluation import predict_event, mae_event, nse_event, ae_event, precision_event, recall_event
from mlflood.evaluation import boxplot_metric, plot_answer_sample, plot_2d_hist
from mlflood.preprocess import folder_split


def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def test(args):

    # load test dataset 
    test_dataset = load_test_dataset(catchment_num=catchment_num, **catchment_kwargs)
    dataloaders_test = dataloader_test(test_dataset, catchment_num = catchment_num, batch_size = 2)

    # get name of test events
    _,_, test_events = folder_split(catchment_num)

    # load model
    path_loc = args.save_dir + experiment_name
    checkpoint = torch.load(path_loc + "/checkpoint.pth")
    model = checkpoint["model"]
    model.eval()

    # predictions: run model for each event
    for event_num in range(2,3):
    #for event_num in range(len((test_dataset).rainfall_events)):
    
        # event name
        event_name = test_events[event_num]

        # predict full event, write the required time to file
        #start_time=time.time()
        pred_event, gt_event, mask = predict_event(model, test_dataset, event_num, 
        start_ts=None, ar = False, ts_out = catchment_kwargs["ts_out"], convlstm=catchment_kwargs["convlstm"])
        
        """
        prediction_time = time.time() - start_time
        with open(args.save_dir + "results/prediction_time.csv", "a", encoding="UTF-8", newline="") as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow([model_name, event_name, prediction_time])
        """

        # delete test dataset
        del test_dataset

        # compute metrics over time -> [T]
        mae_array = mae_event(pred_event, gt_event, mask)
        nse_array = nse_event(pred_event, gt_event, mask)
        abse_array = ae_event(pred_event, gt_event, mask)
        #prop_thd_event=proportional_threshold_event(gt_event)
        #precision_99 = precision_event(pred_event, gt_event, mask, thr=prop_thd_event)
        precision_fix = precision_event(pred_event, gt_event, mask, thr=np.ones(len(mae_array))*0.5)
        recall_fix = recall_event(pred_event, gt_event, mask, thr=np.ones(len(mae_array))*0.5)

        # compute boxplot
        lims = [0.1, 0.2, 0.5, 1]
        mae_boxplot = boxplot_metric(pred_event, gt_event, mask, lims, metric = "mae")

        # metrics computed
        print("metrics computed")

        # save model outputs and metrics
        save_path = path_loc + "/test-" + catchment_num + "/" + event_name + "/"
        if not os.path.exists(save_path):
            os.makedirs(save_path)
        #np.save(save_path + "pred_event.npy", pred_event)
        #np.save(save_path + "gt_event.npy", gt_event)
        np.save(save_path + "mae.npy", mae_array)
        np.save(save_path + "abse.npy", abse_array)
        np.save(save_path + "nse.npy", nse_array)
        np.save(save_path + "precision_fix.npy", precision_fix)
        np.save(save_path + "recall_fix.npy", recall_fix)
        np.save(save_path + "mae_boxplot.npy", mae_boxplot, allow_pickle=True)
        
        print("metrics written")
        del mae_array, nse_array, precision_fix, recall_fix, mae_boxplot

        # plot answer sample and save image
        if (catchment_num == "709_new") or (catchment_num == "709_long"):
            zoom = [500,1000,500,1000]
        elif catchment_num == "744":
            zoom = [2500, 3000, 1500, 2000]
        elif catchment_num == "Luzern":
            zoom = [1500, 2000, 500, 1000]
        for start_timestep in [0, 5, 10, 20]:
            plot_answer_sample(pred_event, gt_event, mask, ts=start_timestep, zoom=zoom, save_folder=save_path, model_name=model_name, start_ts=None, lead_time=catchment_kwargs["lead_time"], ar=False)
            plot_2d_hist(pred_event, gt_event, mask, ts=start_timestep, save_folder=save_path, model_name=model_name)
        print('Done with event ', event_name)
        
        # close all open plots
        plt.close('all')


# argparser
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="training script")

    #### general parameters #####################################################
    parser.add_argument('--tag', default="temp",type=str)
    parser.add_argument("--device",default="cuda",type=str,choices=["cuda", "cpu"])
    parser.add_argument("--save_dir",default="experiments/20220803-exp_lead6-709_new/", 
                        help="Path to directory where models and logs should be saved saved !! this folder must already exist !!")
    parser.add_argument("--logstep-train", default=10,type=int,
                        help="iterations step for training log")
    parser.add_argument("--save-model", default="best",choices=['last','best','No'],help="which model to save")
    parser.add_argument("--val-every-n-epochs", type=int, default=1,help="interval of training epochs to run the validation")
    parser.add_argument('--resume', type=str, default=None,help='path to resume the model if needed')
    parser.add_argument('--mode',default="only_train",type=str,choices=["None","train","test","train_and_test", "only_train"],help="mode to be run")

    #### data parameters ##########################################################

    parser.add_argument("--data",default="709",type=str,choices=["toy", "709", "684", "multi"],help="dataset selection")
    parser.add_argument("--datafolder",type=str,help="root directory of the dataset")
    parser.add_argument("--workers", type=int, default=4,metavar="N",help="dataloader threads")
    parser.add_argument("--batch-size", type=int, default=8)

    #### optimizer parameters #####################################################
    parser.add_argument('--epochs', type=int, default=500)
    parser.add_argument('--optimizer',default='adam', choices=['sgd','adam'])
    parser.add_argument('--lr', type=float, default=0.00001)
    parser.add_argument('--momentum', type=float, default=0.9)
    #parser.add_argument('--momentum', type=float, default=0.99)
    parser.add_argument('--w-decay', type=float, default=0)#1e-5)
    parser.add_argument('--lr-scheduler', type=str, default='multi',choices=['no','step', 'smart', 'multi', 'cyclic'],help='lr scheduler mode')
    parser.add_argument('--lr-step', type=int, default=350,help=' number of epochs between decreasing steps applies to lr-scheduler in [step, exp]')
    parser.add_argument('--lr-gamma', type=float, default=0.1,help='decrease rate')

    #### model parameters #####################################################
    parser.add_argument("--model_name", default='convlstm', type=str,help="model to run: 'cnn', 'unet', 'utae', 'unet3d'")
    parser.add_argument("--loss", default="L1", type=str, help="loss ['MSE', 'L1', 'L1_upd'] ")
    ## training files takes 2 random patches instead of 100 in the normal file. loc: /scratch2/flood_sim/data/709/one_alt/

    ### fix indexes validation
    parser.add_argument("--fix_indexes_val", default=False, const=True, nargs='?', type=str2bool, help="select whether patches are generated sequentially (true) or randomly")
    parser.add_argument("--config", default='experiments/20220803-exp_lead6-709_new/20220803-709_new-simvp-6-False-False-True-convlstm-3/20220803-709_new-simvp-6-False-False-True-convlstm-3.yml', type=str, 
                        help="path to experiment configurations saved in yml file")

    
    args = parser.parse_args()
    #args.seed = random.randint(start, stop)
    print(args)
    
    with open(args.config) as file:
        config = yaml.full_load(file)        
        catchment_kwargs = config['catchment_kwargs']
        catchment_num = config['catchment_num']
        #catchment_num = "709_new"
        optimization_kwargs = config['optimization_kwargs']
        model_name = config['model_name']
        experiment_name = config['experiment_name']
        
    test(args)