import sys, argparse, os
import numpy as np
import random
import yaml
import torch
import torch.nn.functional as F
import datetime
from torch.utils.tensorboard import SummaryWriter
from mlflood.dataset import load_dataset, dataloader_tr_val
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss 
from mlflood.models.CNN_base import CNN_base
from mlflood.models.simvp import SimVP
from mlflood.models.unet import UNet
from mlflood.models.swin_unet.SwinUNet3D import SwinUNet3D
from mlflood.models.convlstm import convlstm_seq
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs
from torchsummary import summary
from mlflood.training import training_loop

def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def train(args):
    # specify directory
    path_loc = args.save_dir + experiment_name

    # create dataset
    train_dataset, valid_dataset = load_dataset(catchment_num=catchment_num, catchment_valid=catchment_num, **catchment_kwargs)
    dataloaders_train, dataloaders_valid = dataloader_tr_val(train_dataset, valid_dataset, 
        catchment_num = catchment_num, batch_size=optimization_kwargs['batch_size'])
    n_input_channels = train_dataset.input_channels

    # initialize optimization parameters
    start_epoch = 1
    best_loss = np.Inf

    # initialize model
    if os.path.exists(path_loc + "/checkpoint.pth"):
        print("Continue training")
        checkpoint = torch.load(path_loc + "/checkpoint.pth")
        model = checkpoint["model"]
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
        start_epoch = checkpoint['epoch'] + 1
        model.load_state_dict(checkpoint['model_state'])
        optimizer.load_state_dict(checkpoint['optimizer_state'])
        best_loss = checkpoint['best_loss']
    elif model_name =="cnn":
        model = CNN_base(border_size=catchment_kwargs['border_size'], timestep=catchment_kwargs['timestep'], n_input_channels=n_input_channels, use_diff_dem=catchment_kwargs['use_diff_dem'], use_topo_index=catchment_kwargs['use_topo_index'], ts_out=catchment_kwargs['ts_out'])
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "unet":
        model = UNet(border_size=catchment_kwargs['border_size'], timestep=catchment_kwargs['timestep'], n_input_channels = n_input_channels, 
        use_diff_dem = catchment_kwargs['use_diff_dem'], use_topo_index=catchment_kwargs['use_topo_index'], ts_out=catchment_kwargs['ts_out'])
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "convlstm":
        model = convlstm_seq(num_channels=n_input_channels)
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "convlstm1":
        model = convlstm_seq(num_channels=n_input_channels, num_layers=1)  
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "simvp":
        model = SimVP(shape_in=[catchment_kwargs['convlstm'], n_input_channels, catchment_kwargs['dim_patch'], catchment_kwargs['dim_patch']])
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "swinunet":
        model = SwinUNet3D(in_chans=n_input_channels, out_chans=1, in_depth=catchment_kwargs['convlstm'], out_depth=catchment_kwargs['ts_out'])
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    elif model_name == "swinunet_light":
        model = SwinUNet3D(in_chans=n_input_channels, out_chans=1, in_depth=catchment_kwargs['convlstm'], out_depth=catchment_kwargs['ts_out'],
        depths=(2,2,3,2), num_heads=(2,4,6,8), mlp_ratio=2)
        optimizer = torch.optim.Adam(model.parameters(), lr = optimization_kwargs["learning_rate"])
    
    # send model to GPU
    model = model.to(args.device)
        
    # train the model
    model, history = training_loop(model, optimizer, dataloaders_train, dataloaders_valid, 
    optimization_kwargs, args.device, path_loc, start_epoch, best_loss)

# argparser
if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="training script")

    #### general parameters #####################################################
    parser.add_argument('--tag', default="temp",type=str)
    parser.add_argument("--device",default="cuda",type=str,choices=["cuda", "cpu"])
    parser.add_argument("--save_dir",default="experiments/20220816-exp_general/", 
                        help="Path to directory where models and logs should be saved saved !! this folder must already exist !!")
    parser.add_argument("--logstep-train", default=10,type=int,
                        help="iterations step for training log")
    parser.add_argument("--save-model", default="best",choices=['last','best','No'],help="which model to save")
    parser.add_argument("--val-every-n-epochs", type=int, default=1,help="interval of training epochs to run the validation")
    parser.add_argument('--resume', type=str, default=None,help='path to resume the model if needed')
    parser.add_argument('--mode',default="only_train",type=str,choices=["None","train","test","train_and_test", "only_train"],help="mode to be run")

    #### data parameters ##########################################################
    parser.add_argument("--data",default="709_new",type=str,choices=["toy", "709", "684", "multi"],help="dataset selection")
    parser.add_argument("--datafolder",type=str,help="root directory of the dataset")
    parser.add_argument("--workers", type=int, default=4,metavar="N",help="dataloader threads")
    parser.add_argument("--batch-size", type=int, default=8)

    #### optimizer parameters #####################################################
    parser.add_argument('--epochs', type=int, default=500)
    parser.add_argument('--optimizer',default='adam', choices=['sgd','adam'])
    parser.add_argument('--lr', type=float, default=0.00001)
    parser.add_argument('--momentum', type=float, default=0.9)
    #parser.add_argument('--momentum', type=float, default=0.99)
    parser.add_argument('--w-decay', type=float, default=0)#1e-5)
    parser.add_argument('--lr-scheduler', type=str, default='multi',choices=['no','step', 'smart', 'multi', 'cyclic'],help='lr scheduler mode')
    parser.add_argument('--lr-step', type=int, default=350,help=' number of epochs between decreasing steps applies to lr-scheduler in [step, exp]')
    parser.add_argument('--lr-gamma', type=float, default=0.1,help='decrease rate')

    #### model parameters #####################################################
    parser.add_argument("--model_name", default='convlstm', type=str,help="model to run: 'cnn', 'unet', 'utae', 'unet3d'")
    parser.add_argument("--loss", default="L1", type=str, help="loss ['MSE', 'L1', 'L1_upd'] ")
    ## training files takes 2 random patches instead of 100 in the normal file. loc: /scratch2/flood_sim/data/709/one_alt/

    ### fix indexes validation
    parser.add_argument("--fix_indexes_val", default=False, const=True, nargs='?', type=str2bool, help="select whether patches are generated sequentially (true) or randomly")

    parser.add_argument("--config", default='experiments/20220713-base_config/20220713-709-None-6-False-False-True/20220713-709-None-6-False-False-True.yml', type=str, 
                        help="path to experiment configurations saved in yml file")

    args = parser.parse_args()
    #args.seed = random.randint(start, stop)
    print(args)
    
    with open(args.config) as file:
        config = yaml.full_load(file)
        catchment_kwargs = config['catchment_kwargs']
        catchment_num = config['catchment_num']
        #catchment_valid=config['catchment_valid']
        optimization_kwargs = config['optimization_kwargs']
        model_name = config['model_name']
        experiment_name = config['experiment_name']
    
    train(args)