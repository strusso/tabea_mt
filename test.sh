#!/bin/bash

# Experiment 3: lead12 Luzer
save_dirs=("experiments/20220803-exp_lead12-Luzern/"
"experiments/20220803-exp_lead12-Luzern/"
"experiments/20220803-exp_lead12-Luzern/"
"experiments/20220803-exp_lead12-Luzern/"
"experiments/20220803-exp_lead12-Luzern/"
"experiments/20220803-exp_lead12-Luzern/")

model_dirs=("experiments/20220803-exp_lead12-Luzern/20220803-Luzern-unet-12-False-False-True/20220803-Luzern-unet-12-False-False-True.yml"
"experiments/20220803-exp_lead12-Luzern/20220803-Luzern-unet-12-False-False-True-timestep-3/20220803-Luzern-unet-12-False-False-True-timestep-3.yml"
"experiments/20220803-exp_lead12-Luzern/20220803-Luzern-convlstm-12-False-False-True/20220803-Luzern-convlstm-12-False-False-True.yml"
"experiments/20220803-exp_lead12-Luzern/20220803-Luzern-swinunet-12-False-False-True/20220803-Luzern-swinunet-12-False-False-True.yml"
"experiments/20220803-exp_lead12-Luzern/20220803-Luzern-simvp-12-False-False-True/20220803-Luzern-simvp-12-False-False-True.yml"
"experiments/20220803-exp_lead12-Luzern/20220803-Luzern-simvp-12-False-False-True-convlstm-3/20220803-Luzern-simvp-12-False-False-True-convlstm-3.yml")

for i in ${!save_dirs[@]};
do
    jobid="job"$i
    echo $jobid
    job="python scripts/test.py --save_dir "${save_dirs[$i]}" --config "${model_dirs[$i]}
    echo $job
    python scripts/test.py --save_dir ${save_dirs[$i]} --config ${model_dirs[$i]}
done


"""
# time it
for i in ${!save_dirs[@]};
do
    start=`date +%s`
    jobid="job"$i
    echo $jobid
    job="python scripts/test.py --save_dir "${save_dirs[$i]}" --config "${model_dirs[$i]}
    python scripts/test.py --save_dir ${save_dirs[$i]} --config ${model_dirs[$i]}
    end=`date +%s`
    echo Execution time was `expr $end - $start` seconds.
done
"""
