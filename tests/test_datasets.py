import numpy as np
from pathlib import Path
from mlflood.conf import PATH_GENERATED
from mlflood.preprocess import build_dataset
from mlflood.dataset import MyCatchment, load_dataset, waterdepth_diff_const, normalize

def test_build_dataset():
    build_dataset("toy")

def test_load_catchment():
    catchment_num = "toy"

    catchment_kwargs = {}
    catchment_kwargs["timestep"]=1
    catchment_kwargs["sample_type"]="single"
    catchment_kwargs["dim_patch"]=64
    catchment_kwargs["border_size"]=0

    gen = MyCatchment(PATH_GENERATED / Path(catchment_num+"-train.h5"), **catchment_kwargs)
    assert(np.sum(np.array(gen.T_steps)-gen.timestep - gen.lead_time)==len(gen))
    for i in [0,1, 18, -2, -1]:
        assert(list(gen[i]['x'].shape) == [8, gen.nx, gen.ny])
        assert(list(gen[i]['y'].shape) == [1, gen.nx, gen.ny])
        assert(list(gen[i]['mask'].shape) == [1, 64, 64])

    # test case for timestep option
    catchment_kwargs = {}
    catchment_kwargs["timestep"]=2
    catchment_kwargs["sample_type"]="single"
    catchment_kwargs["dim_patch"]=46
    catchment_kwargs["border_size"]=3

    gen = MyCatchment(PATH_GENERATED / Path(catchment_num+"-train.h5"), **catchment_kwargs)
    assert(np.sum(np.array(gen.T_steps)-gen.timestep - gen.lead_time)==len(gen))
    for i in [0,1, 18, -2, -1]:
        assert(list(gen[i]['x'].shape) == [10, gen.nx, gen.ny])
        assert(list(gen[i]['y'].shape) == [1, 40,40])
        assert(list(gen[i]['mask'].shape) == [1, 46, 46])

    # test case for lead time option
    catchment_kwargs = {}
    catchment_kwargs["timestep"]=2
    catchment_kwargs["lead_time"]=2
    catchment_kwargs["sample_type"]="single"
    catchment_kwargs["dim_patch"]=46
    catchment_kwargs["border_size"]=3
    catchment_kwargs["use_topo_index"]=True

    gen = MyCatchment(PATH_GENERATED / Path(catchment_num+"-train.h5"), **catchment_kwargs)
    assert(np.sum(np.array(gen.T_steps)-gen.timestep-gen.lead_time)==len(gen))
    for i in [0, 1, 18, -2, -1]:
        assert(list(gen[i]['x'].shape) == [8, gen.nx, gen.ny])
        assert(list(gen[i]['y'].shape) == [1, 40, 40])
        assert(list(gen[i]['mask'].shape) == [1, 46, 46])