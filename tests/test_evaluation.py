
from mlflood.evaluation import predict_dataset, mse_from_predicted_dataset, mses2plot, predict_event
from mlflood.models.Baseline import Baseline
from mlflood.dataset import load_dataset, load_test_dataset
from mlflood.simulation import create_sims_dataset
from poutyne import Model
import torch
import numpy as np
import pandas as pd

def test_mean_predictor():
    catchment_num = "toy"

    for normalize_output in [ True, False]:

        for timestep in [1, 2, 3]:
            for border_size in [0, 1, 3]:
                for use_diff_dem in [True, False]:
                    catchment_kwargs = {}
                    catchment_kwargs["timestep"]=timestep
                    catchment_kwargs["sample_type"]="full"
                    catchment_kwargs["dim_patch"]=120
                    catchment_kwargs["fix_indexes"]=False
                    catchment_kwargs["border_size"] = border_size
                    catchment_kwargs["normalize_output"] = normalize_output    
                    catchment_kwargs["use_diff_dem"] = use_diff_dem    

#                     mean_model = base_model(border_size = border_size, normalize_output=normalize_output, use_diff_dem=use_diff_dem)
                    mean_model = Baseline(border_size=border_size, normalize_output=normalize_output, use_diff_dem=use_diff_dem)

                    for dataset in load_dataset(catchment_num=catchment_num, **catchment_kwargs):
                        prediction_ag = predict_dataset(mean_model, dataset, ar=True)
                        true_y = []
                        for i in range(dataset.N_events):
                            true_y.append(dataset.waterdepth[i][timestep:].numpy())

                        true_y = np.array(true_y)
                        if border_size:
                            true_y = true_y[:, :, border_size:-border_size, border_size:-border_size]
                        y_pred = np.array([e[0] for e in prediction_ag])
                        y = np.array([e[1] for e in prediction_ag])
                        np.testing.assert_allclose(y_pred, 0)

                        np.testing.assert_allclose(y, true_y)

                        mse = np.array(mse_from_predicted_dataset(prediction_ag))
                        mse2 = np.mean(y**2, axis=(2,3))

                        np.testing.assert_allclose(mse, mse2, rtol=1e-6, atol=1e-6)

def test_mean_predictor_1step():

    catchment_num = "toy"
    use_diff_dem = True
    for normalize_output in [ True, False]:

        for timestep in [1, 2, 5]:
            for b in [0, 2, 4]:
                catchment_kwargs = {}
                catchment_kwargs["timestep"]=timestep
                catchment_kwargs["sample_type"]="full"
                catchment_kwargs["dim_patch"]=120
                catchment_kwargs["fix_indexes"]=False
                catchment_kwargs["border_size"] = b
                catchment_kwargs["normalize_output"] = normalize_output    
                catchment_kwargs["use_diff_dem"] = use_diff_dem    

                mean_model = Baseline(border_size=b, normalize_output=normalize_output, use_diff_dem=use_diff_dem)

                for dataset in load_dataset(catchment_num=catchment_num, **catchment_kwargs):
                    print(normalize_output, timestep, b)
                    prediction_ag = predict_dataset(mean_model, dataset, ar=False)
                    true_y_pred = []
                    true_mse = []
                    for i in range(dataset.N_events):
                        true_y_pred.append(dataset.waterdepth[i][timestep-1:-1].numpy())
                        if b:
                            true_mse.append(np.mean(np.diff(dataset.waterdepth[i][timestep-1:,b:-b, b:-b].numpy(), axis=0)**2, axis=(1,2)))
                        else:
                            true_mse.append(np.mean(np.diff(dataset.waterdepth[i][timestep-1:].numpy(), axis=0)**2, axis=(1,2)))
                    true_y_pred = np.array(true_y_pred)
                    true_mse = np.array(true_mse)
                    if b:
                        true_y_pred = true_y_pred[:, :, b:-b, b:-b]
                    y_pred = np.array([e[0] for e in prediction_ag])
                    y = np.array([e[1] for e in prediction_ag])
                    np.testing.assert_allclose(y_pred, true_y_pred)

                    mse = np.array(mse_from_predicted_dataset(prediction_ag))

                    np.testing.assert_allclose(mse, true_mse, rtol=1e-8, atol=1e-8)
               

