


import torch
import numpy as np

from poutyne import Model, SKLearnMetrics
from sklearn.metrics import r2_score, explained_variance_score, mean_squared_error, median_absolute_error
from mlflood.dataset import load_dataset
from mlflood.models.CNN_base import CNN_base
from mlflood.conf import PATH_SUMMARY, PATH_CHECKPOINTS
from pathlib import Path
from mlflood.dataset import waterdepth_diff_const
from mlflood.simulation import create_sims_dataset
from mlflood.evaluation import predict_batch
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test
from mlflood.training import training_loop
from mlflood.loss import l1_loss_weight, l2_loss, l1_loss, l1_loss_multiple_outs


def mse(p):
    """Compute MSE"""
    mses = []
    for v1, v2 in p:
        mses.append(np.mean((v1-v2)**2))
    return mses

def test_general():
    cuda_device = 0
    device = torch.device("cuda:%d" % cuda_device if torch.cuda.is_available() else "cpu")
    create_sims_dataset(32, 32, [4,2,3], "testsims")
    catchment_num = "testsims"
    experiment_name = "tmp"
    normalize_output = True
    timestep = 1
    border_size = 0
    ts_out = None

    # parameter for the catchment
    catchment_kwargs = {}
    catchment_kwargs["tau"]=0.5
    catchment_kwargs["timestep"]=timestep
    catchment_kwargs["sample_type"]="single"
    catchment_kwargs["dim_patch"]=16
    catchment_kwargs["fix_indexes"]=True
    catchment_kwargs["border_size"]=border_size
    catchment_kwargs["normalize_output"]=normalize_output
    catchment_kwargs["ts_out"] = ts_out 

    # optimization paramters
    optimization_kwargs = {}
    optimization_kwargs["batch_size"] = 8
    optimization_kwargs["epochs"] = 2
    optimization_kwargs["learning_rate"] = 0.0001
    optimization_kwargs["weight_d"] = 0.0001 
    
    train_dataset, valid_dataset = load_dataset(catchment_num=catchment_num, **catchment_kwargs)
    dataloaders_train, dataloaders_valid = dataloader_tr_val(train_dataset, valid_dataset, catchment_num = "709", batch_size = 8)
    
    network = CNN(border_size=border_size, timestep = timestep)
    network = network.to(device)
    model, history = training_loop(network, l1_loss, dataloaders_train, dataloaders_valid, optimization_kwargs, device, path_loc = None)

#     mse1, _ = model.evaluate_dataset(valid_dataset)
#     if normalize_output:
#         mse1 = mse1 * waterdepth_diff_const

#     mse2 = np.mean(np.array(mse(predict_batch(model, valid_dataset))))

    # np.testing.assert_allclose(mse1, mse2, rtol=1e-6)

    tr_loss = history['train_loss'][0]
    assert isinstance(tr_loss, (int, float))