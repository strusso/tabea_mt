import numpy as np
from mlflood.simulation import create_random_dem, simulate, one_step, create_sims_dataset
import torch
from mlflood.dataset import load_dataset, pad_borders, unnormalize
from mlflood.models.PerfectGraph import PerfectGraph
from mlflood.dataset import load_dataset, load_test_dataset, dataloader_tr_val, dataloader_test


# def test_one_step():
#     dem = np.zeros([3,3])
#     rf = 1
#     cwd = np.zeros([3,3])
#     nwd, leakage = one_step(cwd, rf, dem)
#     ewd = np.array([[1/3, 1/2, 1/3], [1/2, 1, 1/2], [1/3, 1/2, 1/3]])
#     np.testing.assert_allclose(nwd, ewd)
#     np.testing.assert_allclose(leakage, 4*(1/2+2/3))

#     dem = np.zeros([3,3])
#     rf = 1
#     cwd = np.ones([3,3])
#     nwd, leakage = one_step(cwd, rf, dem)
#     ewd = 2*np.array([[1/3, 1/2, 1/3], [1/2, 1, 1/2], [1/3, 1/2, 1/3]])
#     np.testing.assert_allclose(nwd, ewd)
#     np.testing.assert_allclose(leakage, 2*4*(1/2+2/3))


def test_simulation():
    
    for i in range(100):
        
        px = np.random.randint(1, 10)
        py = np.random.randint(1, 7)
        T = np.random.randint(1, 10)

        dem = create_random_dem(px, py)

        np.testing.assert_array_less(dem, 1+1e-10)
        np.testing.assert_array_less(-1e-10, dem)

        np.testing.assert_equal(dem.shape, [px,py])

        cwd = np.random.rand(px, py)
        rf = 1
        nwd, leakage = one_step(cwd, rf, dem)
        np.testing.assert_allclose(np.sum(nwd)+ leakage , np.sum(cwd+rf))
        np.testing.assert_equal(nwd.shape, [px,py])


        rainfall = np.random.rand(T)
        wd, leakage = simulate(rainfall, dem)

        np.testing.assert_equal(wd.shape, [T+1, px, py])

        tw = np.sum(np.sum(wd[1:], axis=2), axis=1) + np.cumsum(leakage)
        np.testing.assert_allclose(tw, np.cumsum(rainfall)*dem.shape[0]*dem.shape[1])
        np.testing.assert_array_less(-1e-10, wd)
        np.testing.assert_array_less(-1e-10, leakage)


def test_dem():

    dem = create_random_dem(1, 1)

    np.testing.assert_array_less(dem, 1+1e-10)
    np.testing.assert_array_less(-1e-10, dem)

    dem = create_random_dem(2, 1)

    np.testing.assert_array_less(dem, 1+1e-10)
    np.testing.assert_array_less(-1e-10, dem)

    dem = create_random_dem(1, 2)

    np.testing.assert_array_less(dem, 1+1e-10)
    np.testing.assert_array_less(-1e-10, dem)


def test_generate():
    # Create the dataset (to be runned only once)
    create_sims_dataset(32, 32, [4,2,3], "testsims")
