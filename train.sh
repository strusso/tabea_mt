#!/bin/bash
"""
# 24 argument save dirs
save_dirs=(
"experiments/20220803-exp_lead6-744/"  
)

# directions for SIMVP3
model_dirs=("experiments/20220803-exp_lead6-744/20220803-744-simvp-6-False-False-True-convlstm-3/20220803-744-simvp-6-False-False-True-convlstm-3.yml")

"""
save_dirs=(
"experiments/20220912-exp_lead12-709_long/"
"experiments/20220912-exp_lead12-709_long/"
"experiments/20220912-exp_lead12-709_long/"
)

model_dirs=("experiments/20220912-exp_lead12-709_long/20220912-709_long-swinunet-12-False-False-True-convlstm-5/20220912-709_long-swinunet-12-False-False-True-convlstm-5.yml"
"experiments/20220912-exp_lead12-709_long/20220912-709_long-simvp-12-False-False-True-convlstm-5/20220912-709_long-simvp-12-False-False-True-convlstm-5.yml"
"experiments/20220912-exp_lead12-709_long/20220912-709_long-simvp-12-False-False-True-convlstm-3/20220912-709_long-simvp-12-False-False-True-convlstm-3.yml")

names=("Swinunet60-5" "SimVP60-5" "SimVP60-3")

for i in ${!save_dirs[@]};
do
    job="python scripts/train.py --save_dir "${save_dirs[$i]}" --config "${model_dirs[$i]}
    jobid=${names[$i]}
    echo $jobid
    bsub -R "rusage[mem=90000, ngpus_excl_p=1]" -W 120:00 -J $jobid $job
done

